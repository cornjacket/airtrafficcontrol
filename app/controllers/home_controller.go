package controllers

import (
	"net/http"

	"bitbucket.org/cornjacket/airtrafficcontrol/app/responses"
)

func (app *AppContext) Home(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "Service Up")

}
