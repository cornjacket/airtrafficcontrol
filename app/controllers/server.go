package controllers

import (
	"fmt"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"bitbucket.org/cornjacket/airtrafficcontrol/app/services"
	"bitbucket.org/cornjacket/iot/serverlib/tallocrespproc"
)

type TallocRespPostInterface interface {
        Open(Hostname, Port, Path string) (error)
	SendTallocResp(resp tallocrespproc.TallocResp) (error)
}

// TODO: change guild so that it doesn't use package level scope. Instead create a type which lives in the AppContext
type AppContext struct {
	Router *httprouter.Router
	TallocRespService TallocRespPostInterface //services.TallocRespService
}

func NewAppContext() *AppContext {
        context := AppContext{}
        context.TallocRespService = &services.TallocRespService{} // implements ViewHandlrPostInterface
        return &context
}


func (app *AppContext) Run(addr string) {
        // Instantiate a new router
        app.Router = httprouter.New()

        app.initializeRoutes()

        fmt.Printf("Listening to port %s\n", addr)
        log.Fatal(http.ListenAndServe(":"+addr, app.Router))
}

