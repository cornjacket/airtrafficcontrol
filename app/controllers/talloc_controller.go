package controllers

// TODO: I need to create a enum for the treqResp error codes.


import (
	// Standard library packets
	"fmt"
	"net/http"

	// Third party packages
	"github.com/julienschmidt/httprouter"
	nodestate "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes"
	"bitbucket.org/cornjacket/airtrafficcontrol/app/tallocreqproc"
	"bitbucket.org/cornjacket/iot/serverlib/tallocrespproc"
)


func (app *AppContext) NewTslotAllocWorkerPoolFunc(numWorkers int)  func(http.ResponseWriter, *http.Request, httprouter.Params) {
        return tallocreqproc.NewTAllocReqHandlerFunc(numWorkers, app.tAllocReqHandler)
}

// How to handle the treq.TimeReq field? The time allocation should be based on the time required by the node. Currently the allocation 
// is based on the gw_packet recevied prior to this tslot allocation request. This will need to change if applications require more
// than one time slot.
// 0. Assumption is that gw_packet has already reached airtrafficcontrol and the time allocation has already happened.
// 1. Receive the TAllocReq
// 2. Lookup nodestate to see if entry exists and if tslot is valid. The normal case will not update the nodestate so a lock
//    is not needed. 
// 3. If so, then send response, otherwise send error code back to cmd_handler, potentially create nodestate entry with error and w/o gateway info 
func (app *AppContext) tAllocReqHandler(wID int, treq tallocreqproc.TAllocReq) bool {

	tallocreqproc.DisplayInfoMessageWithLatency("talloc req recvd", wID, &treq)
	var nodeState *nodestate.Atc_Nodes
	var nodeFound bool
	var err error
	if validateReq(&treq) {

		// err means that node row was not created in nodeState table
		nodeState, nodeFound, err = tallocFetchNodeOrCreateDefaultIfNoExist(treq.NodeEui)
		if err != nil {
			//fmt.Printf("ATC: treqResp error -1. nodeState database error: %v\n", err)
			err = app.tAllocSendErrorResponse(wID, &treq, -1)
			tallocreqproc.DisplayPassFailMessageWithLatency("sent treqResp error -1 (nodeState db error) to requesting app", err, wID, &treq, true)
			return false
		}

		if nodeFound {

			if nodeState.ValidTslot.Get() == 1 {
				// typical case
				err = app.tAllocSendResponse(wID, &treq, nodeState.AssignedTslot.Get())
				tallocreqproc.DisplayPassFailMessageWithLatency("sent treqResp with alloc tslot to requesting app", err, wID, &treq, true)
			} else {
				//fmt.Printf("ATC: treqResp error -2. node found but tslot marked invalid.\n")
				err = app.tAllocSendErrorResponse(wID, &treq, -2)
				tallocreqproc.DisplayPassFailMessageWithLatency("sent treqResp error -2 (node found but tslotValid=false) to requesting app", err, wID, &treq, true)
			}

		} else {
			// this handler is unable to create a new tslot allocation because it does not know the gateway.
			// the best we can do is send an error to the request source
			fmt.Printf("ATC: treqResp error -3. nodeState not found. unable to determine tslot\n")
			err = app.tAllocSendErrorResponse(wID, &treq, -3)
			tallocreqproc.DisplayPassFailMessageWithLatency("sent treqResp error -3 (nodeState found but unable to determine tslot) to requesting app", err, wID, &treq, true)
		}

		nodeState.Close(err)

	} else {
		//fmt.Printf("ATC: treqResp error -4. Bad TallocReq\n")
		err = app.tAllocSendErrorResponse(wID, &treq, -4)
		tallocreqproc.DisplayPassFailMessageWithLatency("sent treqResp error -4 (bad TallocReq) to requesting app", err, wID, &treq, true)
	}

	return true
}

// TODO: Review what other fields should be validated
func validateReq(treq *tallocreqproc.TAllocReq) bool {
	if treq.NodeEui == "" {
		return false
	}
	return true 
}

func (app *AppContext) tAllocSendResponse(wID int, treq *tallocreqproc.TAllocReq, tslot int) error {
	tresp := tallocrespproc.TallocResp{	NodeEui: treq.NodeEui,
					 	Code: 1, 
						Ts: treq.Ts, 
						CorrId: treq.CorrId, 
						ClassId: treq.ClassId, 
						Hops: treq.Hops, 
						Tslot: tslot }
	err := app.TallocRespService.SendTallocResp(tresp)
	return err
}

func (app *AppContext) tAllocSendErrorResponse(wID int, treq *tallocreqproc.TAllocReq, error_code int) error {
	tresp := tallocrespproc.TallocResp{	NodeEui: treq.NodeEui,
					 	Code: error_code, 
						Ts: treq.Ts, 
						CorrId: treq.CorrId, 
						ClassId: treq.ClassId, 
						Hops: treq.Hops}
	err := app.TallocRespService.SendTallocResp(tresp)
	return err
}

// TODO: This function will create a default row in nodeStates if a row with eui DNE, with validOnboard=false. Therefore no fields except for Eui can be trusted.
func tallocFetchNodeOrCreateDefaultIfNoExist(nodeEui string) (*nodestate.Atc_Nodes, bool, error) {
	nodeState := nodestate.New()

	createRowIfNotFound := true // TODO: This should be a constant value defined in the lib.
	lockRow := false 
	err, nodeFound := nodeState.GetByEui(nodeEui, createRowIfNotFound, lockRow) // TODO: Order of these return values is not standard // Returns a pointer to the nodeState object model
	if err != nil {
		fmt.Printf("ATC: tallocResp error 1. database error: %v\n", err) // TODO: we don't take any action. We should notify an alert service
	} 
	return nodeState, nodeFound, err
}
