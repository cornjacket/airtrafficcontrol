package controllers

import (
	// Standard library packets
	"encoding/json"
	"fmt"
	"net/http"

	// Third party packages
	"github.com/julienschmidt/httprouter"
	"bitbucket.org/cornjacket/airtrafficcontrol/app/atc_gateways"
)


type NewGatewayReq struct {
        Eui  string // Gateway Eui
        Version int
        Ts   uint64
        Name string
        Subband int
        Cardinality int // LoRa network cardinality
        TslotType int // Can be used to select between different tslot size
        NetworkId int
}

// TODO: This function should generate a new entry in the atc_gateways table. 
func (app *AppContext) CreateGateway(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	// Stub an example packet to be populated from the body
	req := NewGatewayReq{}

	// Populate the packet data
	json.NewDecoder(r.Body).Decode(&req)

	// Marshal provided interface into JSON strucutre
	reqj, _ := json.Marshal(req)

	fmt.Printf("ATC.Ctl.CreateGateway: %v", req)

	gateway := atc_gateways.Row{}
	gateway.ValidOnboard = 1
	gateway.Eui = req.Eui
	gateway.TslotType = 0 // only support 15 sec period
	gateway.TotalTslots = 232
	gateway.AllocatedTslots = 0
	if req.Subband == 0 {
		req.Subband = 1
	}
	gateway.Subband = req.Subband
	gateway.NextAvailableTslot = 2
	gateway.MaxNodesPerTslot = 4
	gateway.NetworkId = req.NetworkId
	gateway.CountInCurrentTslot = 0

// TODO: Need to add an insert. It should fail if the gateway already exists. Gateway ID should be unique...

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)
	fmt.Fprintf(w, "%s", reqj)
}
