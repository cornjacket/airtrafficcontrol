package controllers

import (
	// Standard library packets
	//"encoding/json"
	"fmt"
	"net/http"

	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
	"bitbucket.org/cornjacket/iot/loriot"
	"bitbucket.org/cornjacket/iot/serverlib/pktproc2"
	"bitbucket.org/cornjacket/airtrafficcontrol/app/utils/parse"
	"github.com/julienschmidt/httprouter"
	nodestate "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes"
	gatewaystate "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_gateways"
	tslotalloc "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_tslotalloc"
)

func (app *AppContext) NewParserWorkerPoolFunc(numWorkers int)  func(http.ResponseWriter, *http.Request, httprouter.Params) {
	return pktproc2.NewPacketHandlerFunc(numWorkers, app.packetHandler)

}

/* Can I re-use this function
// TODO: This function belongs in a client lib until some of the discrepancies between upPacket and nodeRow are resolved
func packet2nodeState(p *message.UpPacket, nodeState *nodestate.NodeState_Appnum4Appapi3Sysapi6, markInvalid bool) {
	//nodeState.Eui.Set(p.Eui) // =		p.Eui
	nodeState.LastPktRcvd.Set(p.Data)
	nodeState.LastPktRcvdTs.Set(int64(p.Ts))
	nodeState.LastRssi.Set(p.Rssi)
	nodeState.LastSnr.Set(int(p.Snr*10))
	if markInvalid == true {
		nodeState.ValidOnboard.Set(0)
	}
	// The following need to be added to the structure
	//nodeRow.LastDr =	p.Dr
	//if p.Ack {
	//	nodeRow.LastAck = 1
	//}
	//nodeRow.LastCmd =	p.Cmd
	//nodeRow.LastFcnt =	p.Fcnt
	//nodeRow.LastFreq =	int64(p.Freq) // TODO: Is p.Freq too small to hold the freq?
	//nodeRow.LastPort =	p.Port
	//
}
*/

func (app *AppContext) packetHandler(wID int, packet message.UpPacket) bool {


	//loraPktSlice, validPacket := validatePacket(packet)
	_, validPacket := validatePacket(packet) // Bring back the slice if it is needed somewhere
	if validPacket {
		subband := freqToSubband(packet.Freq)
		// HANDLE GATEWAYSTATE
		gateway_list := packet.Gws
		gatewayStates, gatewaysFound, err := checkIfAllGatewaysExistInGatewayStateTable(gateway_list, subband) // probably need to pass in the upPacket

		if err != nil {
			pktproc2.DisplayPassFailMessageWithLatency("Check existence of gateways in gatewway state table", err, wID, &packet, false) // TODO: we don't take any action. We should notify an alert service
			return false
		}

		// gatewayFound indicates whether the gateway existed prior to the GetByEui() call. It can be used to determined what scenario needs to be handled
		gatewayFound := gatewaysFound[0] // For now we are going to focus on just single gateway scenario
		gatewayState := gatewayStates[0] // For now we are going to focus on just single gateway scenario

		// err means that node row was not created in nodeState table
		nodeState, nodeFound, err := fetchNodeOrCreateDefaultIfNoExist(packet.Eui, subband, gatewayStates)
		pktproc2.DisplayPassFailMessageWithLatency("Fetch node/create default if not exist", err, wID, &packet, true)
		if err != nil {
			//fmt.Printf("ATC: upPacket error 2A. database error: %v\n", err) // TODO: we don't take any action. We should notify an alert service
			return false // TODO: Is this the correct approach here? Should we return false or should we do something else prior. I.e. notify another servie indicating database error
		}
		updateNodeState(nodeState, gatewayState, packet) // what about error result

		//fmt.Printf("PKT_RCVR\tINFO\tPktFwdPktHandler: worker %d, gws: %v\n", wID, packet.Gws)

		if nodeFound && gatewayFound {

			// typical case
			fmt.Printf("PKT_RCVR\tINFO\tnodeFound && gatewayFound\n")
			// TODO: Confirm Gateway is Node's Primary or Secondary Gateway
			// TODO: If not, check tslot_alloc table if entry exists for 
			verifyPacketWithinTslot()
			verifyPacketWithinSubband()

		} else if !nodeFound && gatewayFound {

			// new Node onboarding to existing gateway case
			fmt.Printf("PKT_RCVR\tINFO\t!nodeFound && gatewayFound\n")
			newTslotAllocation(nodeState, gatewayState)

		} else if nodeFound && !gatewayFound {

			// existing Node upstreaming to new gateway
			fmt.Printf("PKT_RCVR\tINFO\tnodeFound && !gatewayFound\n")
			// TODO: Create new gatewayState row but turn off simple allocation. And create tslot allocation for the node_gateway pair for the node's tslot

		} else {

			// initial new Node onboarding to new Gateway case - !nodeFound && !primaryGatwayFound
			fmt.Printf("PKT_RCVR\tINFO\t!nodeFound && !gatewayFound\n")
			newTslotAllocation(nodeState, gatewayState)

		}

		verifyPrimaryAndSecondaryGatewayAreNotSame(nodeState) // Should also verify that the PrimaryGateway is not ""
		setSecondaryGatewayScenario(nodeState, gateway_list) // TODO: This needs to be reviewed
		verifyPacketWithinTslot()
		verifyPacketWithinSubband()

		updateUpPacketCount(gateway_list) // probably need to pass a point to the slice, also need to pass the nodeState handle

		// Save node state to database
		_, err = nodeState.Update() // TODO: Add parameter to Update(bool) which executes a commit or rollback if bool is true
		pktproc2.DisplayPassFailMessageWithLatency("Update nodeState", err, wID, &packet, true)
		if err != nil {
			nodeState.Close(err) // release the lock TODO: Add back after testing using open
			return false // TODO: Should we return here prior to gatewayState.Update or should we save the return value and then attempt the Update below
		}
		nodeState.Close(err) // release the lock

		// Save gateway state to database
		_, err = gatewayState.Update()
		pktproc2.DisplayPassFailMessageWithLatency("Update gatewayState", err, wID, &packet, true)
		if err != nil {
			//fmt.Printf("ATC: upPacket error 4. gatewayState.Update. database error: %v\n", err)
			gatewayState.Close(err) // release the lock TODO: Add back after testing using open
			return false // TODO: See question above.
		}
		gatewayState.Close(err) // release the lock

	} else {
		fmt.Printf("PKT_RCVR\tINFO\tPktFwdPktHandler: worker %d: Not processing packet. cmd!=gw\n", wID)
	}

	return true // TODO: What does return true mean in this context
}

func validatePacket(p message.UpPacket) ([]byte, bool) {
	if loriot.IsGwPacket(p) == false {
		return nil, false
	}
	return  parse.HexStringToSlice(p.Data)
}

// DRT - This func should go into a system clib
// TODO: Need to parse packet and return bool
func isCloudSyncPkt(opcode byte) bool {
	return opcode == 0xFF
}

func fetchNodeOrCreateDefaultIfNoExist(nodeEui string, subband int, gatewayStates []*gatewaystate.Atc_Gateways) (*nodestate.Atc_Nodes, bool, error) {
	nodeState := nodestate.New()
	//currentTable := nodeState.GetCurrentTableName()
	//fmt.Printf("ATC: pktRcvr: currentTable: %s, CreateTableString: %s\n", currentTable, nodeState.CreateTableString())

	createRowIfNotFound := false // TODO: This should be a constant value defined in the lib.
	lockRow := true
	err, nodeFound := nodeState.GetByEui(nodeEui, createRowIfNotFound, lockRow) // TODO: Order of these return values is not standard // Returns a pointer to the nodeState object model
	// TODO: This logic can be rearranged. It is somewhat clumsy.
	//if err != nil {
	//	fmt.Printf("ATC: upPacket error 2B. database error: %v\n", err) // TODO: we don't take any action. We should notify an alert service
	//} else
	 if err == nil && !nodeFound {
		//fmt.Printf("ATC: upPacket node NOT found. Going to create Row\n")
		err = createNewNodeRow(nodeEui, nodeState, subband, gatewayStates) // TODO: We dont take any action
		// TODO: If lockRow is true then after creating the row, there should be a slect afterwards
	}
	return nodeState, nodeFound, err
}

// Func called on nodeState for every packet received via first received gateway
func updateNodeState(nodeState *nodestate.Atc_Nodes, gatewayState *gatewaystate.Atc_Gateways, packet message.UpPacket) {
	nodeState.LastPktRcvd.Set(packet.Data)
	nodeState.LastPktTs.Set(int64(packet.Ts))
	nodeState.LastGateway.Set(gatewayState.Eui.Get())
}

func recordTslotAlloc(nodeState *nodestate.Atc_Nodes, gatewayState *gatewaystate.Atc_Gateways, nextAvailableTslot int) error {
	var tslotAllocRow tslotalloc.Row
	tslotAlloc := tslotalloc.New()
	tslotAllocRow.Eui = nodeState.Eui.Get()
	tslotAllocRow.GatewayEui = gatewayState.Eui.Get()
	tslotAllocRow.Tslot = nextAvailableTslot
	err := tslotAlloc.Insert(&tslotAllocRow)
	return err
}

func newTslotAllocation(nodeState *nodestate.Atc_Nodes, gatewayState *gatewaystate.Atc_Gateways) {

	// There are two ways to do this. 1) simple round robin allocation if in simple mode. 2) query tslot_alloc table and determine next available
	// For now something real simple
	if gatewayState.SimpleAllocation.Get() != 0 {
		nextAvailableTslot := gatewayState.NextAvailableTslot.Get()
		nodeState.AssignedTslot.Set(nextAvailableTslot)
		nodeState.ValidTslot.Set(1)
		gatewayState.AllocatedTslots.Set(gatewayState.AllocatedTslots.Get() + 1)
		gatewayState.CountInCurrentTslot.Set(gatewayState.CountInCurrentTslot.Get() + 1)
		if gatewayState.CountInCurrentTslot.Get() >= gatewayState.MaxNodesPerTslot.Get() {
			gatewayState.NextAvailableTslot.Set(gatewayState.NextAvailableTslot.Get() + 1)
			gatewayState.CountInCurrentTslot.Set(0)
		}
		err := recordTslotAlloc(nodeState, gatewayState, nextAvailableTslot) // TODO: I will need to add this same mechanism below in the else clause for complex allocation
		if err != nil {
			fmt.Printf("PKT_RCVR\tUpdate to tslotAlloc table failed.\n")
		}
	} else {
		// TODO: we need to figure out how to handle this condition. Probably query all tslot allocations to see if there is an
		// open tslot available to use. If not, then we need to oversubscribe intelligently and we need to generate an alert..
		fmt.Printf("PKT_RCVR\tINFO\tTslotAllocation has filled up. Currently we don't have a solution for this yet.\n")
		gatewayState.AllocatedTslots.Set(gatewayState.AllocatedTslots.Get() + 1)
	}
	if gatewayState.AllocatedTslots.Get() >= gatewayState.TotalTslots.Get() {
		gatewayState.SimpleAllocation.Set(0)
	}

}

// TODO: I need to finish this implementation. This function should be part of the LoRa library.
func freqToSubband(frequency uint64) int {
	return 1
}

//gatewayStates := checkIfAllGatewaysExistInGatewayStateTable(gateway_list, upstream_subband) // probably need to pass in the upPacket
// iterate through all gateways to check if each exists, row is created with good defaults if row doesn't exist. if not initially found, then found returns false
// returns slice of *atc_gateways and slice of bool whether gateway was found in table
func checkIfAllGatewaysExistInGatewayStateTable(gatewayEuiList []message.GatewayDescriptor, subband int) ([]*gatewaystate.Atc_Gateways, []bool, error) { // probably need to pass in the upPacket

	// TODO: Should check length of gatewayEuiList	
	//gatewayStates := []*gatewaystate.Atc_Gateways{}
	var found []bool
	var gatewayStates []*gatewaystate.Atc_Gateways
	// for each gateway in gatewayEuiList
	//	gatewayFound := findGatewayInGatewayStateTable(gateway) // if not found then a gatway record will automatically be created and returned, this will make the gatewayOnboard invalid since it was not explicitly onboarded before packet Rx
	//	if !gatewayFound
	//		updateGatewayWithsubband(subband) // gateway has invalidOnboard
	//gatewayStates[0] = gatewaystate.New() // Is this legal way to add to slice??????
	gatewayStates = append(gatewayStates, gatewaystate.New())
	found = append(found, false)
	//////////////////////////////////////////////
	// NEW STUFF UNDER TEST

		//currentTable := gatewayStates[0].GetCurrentTableName()
		//fmt.Printf("ATC: pktRcvr: GW-currentTable: %s, GW-CreateTableString: %s\n", currentTable, gatewayStates[0].CreateTableString())
		gatewayEui := gatewayEuiList[0].GwEui

		createRowIfNotFound := false // TODO: This should be a constant value defined in the lib so that it has explicit meaning, i.e. gatewaystate.DoNotCreateRowIfNotFound
		lockRow := true
		err, gatewayFound := gatewayStates[0].GetByEui(gatewayEui, createRowIfNotFound, lockRow) // TODO: Order of these return values is not standard
		////////////////////////////////////////////////////////////////////////////////////////

		if err != nil {
			//fmt.Printf("ATC: upPacket error 1B. database error: %v\n", err) // TODO: we don't take any action. We should notify an alert service
			//return false // what does returning false mean - what will thread pool do? 
		} else if gatewayFound {
			//fmt.Printf("ATC: upPacket gateway found\n")
			found[0] = true
		} else {
			err = createNewGatewayRow(gatewayEui, gatewayStates[0], subband)
			//fmt.Printf("ATC: DEBUG 3\n")
		}
	//////////////////////////////
	return gatewayStates, found, err
}

// create gateway row with good default values
// Do not make TslotValid = 1 for insertion so that default Tslot in row is not used until Tslot is assigned in a later step.
func createNewNodeRow(nodeEui string, nodeState *nodestate.Atc_Nodes, subband int, gatewayStates []*gatewaystate.Atc_Gateways) error {
	var nodeRow nodestate.Row
	nodeRow.Eui = nodeEui
	nodeRow.Subband = subband
	if len(gatewayStates) >= 1 {
		nodeRow.PrimaryGateway = gatewayStates[0].Eui.Get()
	}
	if len(gatewayStates) >= 2 {
		nodeRow.SecondaryGateway = gatewayStates[1].Eui.Get()
	}
	err := nodeState.Insert(&nodeRow)
	return err
}

// create gateway row with good default values
func createNewGatewayRow(gatewayEui string, gatewayState *gatewaystate.Atc_Gateways, subband int) error {
	var gatewayRow gatewaystate.Row
	gatewayRow.Eui = gatewayEui
	gatewayRow.SimpleAllocation = 1 // A new gateway enables simple allocation, however this may not be the case for multi-gateway scenario 
	gatewayRow.TslotType = 0 // TODO: This should be a constant defined in this file so that it has meaning. Basically 15 sec tslot
	gatewayRow.TotalTslots = 60 - 8
	gatewayRow.AllocatedTslots = 0
	gatewayRow.Subband = subband
	gatewayRow.NextAvailableTslot = 2
	gatewayRow.MaxNodesPerTslot = 4
	gatewayRow.CountInCurrentTslot = 0
	err := gatewayState.Insert(&gatewayRow)
	return err
}

// verify Primary and Secondary Gateways are not same, and if they are then invalidate the Secondary Gateway info
// TODO: Why is it important to set secodary to "" if primary and secondary gateway eui's match. It looks like an error case that wasn't
//		correctly handled.
func verifyPrimaryAndSecondaryGatewayAreNotSame(nodeState *nodestate.Atc_Nodes) {
	if nodeState.PrimaryGateway.Get() != "" && nodeState.SecondaryGateway.Get() != "" {
		if nodeState.PrimaryGateway.Get() == nodeState.SecondaryGateway.Get() {
			nodeState.SecondaryGateway.Set("")
		}
	}
}

func verifyPacketWithinTslot() {
	// TODO: make sure tslot is not more than one tslot off in either direction. - indicates that drift correction is broken
	// TODO: make sure that node is not out of tslot for more than X consecutive wakes - indicates that drift correction turned off 
}

func verifyPacketWithinSubband() {
	// TODO: compare packet's subband with nodestate's subband
}

// TODO: Is this needed? Is the plan to count number of packets through gateway, then that means I need to lock the gateway row. Maybe better idea is to track
// the number of packets on a tslotalloc basis, i.e. { gatwayeui, nodeeui, upcount }. Then total packets can be aggregated.
// compare each gateway that packet was received on with primary and secondary gateways and increment corresponding counts
// need an other_gateway_pkt_count in database to handle where there are more than 2 gateways in range
func updateUpPacketCount(gateway_list []message.GatewayDescriptor) { // probably need to pass a point to the slice, also need to pass the nodeState handle

}

// TODO: Review the logic here. Looks like Primary and Secondary could become the same if we don't check for that condition.
// If the Primary Gateway exists but Secondary doesnt, then set the secondary gateway by inspecting the gatweay_list
func setSecondaryGatewayScenario(nodeState *nodestate.Atc_Nodes, gateway_list []message.GatewayDescriptor) { // TODO: probably need to pass a point to the slice
	if (nodeState.SecondaryGateway.Get() == "") && (len(gateway_list) > 1) {
		nodeState.SecondaryGateway.Set(gateway_list[1].GwEui)
	}

}

