package controllers

import (
	"net/http"
	"github.com/julienschmidt/httprouter"
	"github.com/gorilla/context"
)

func (app *AppContext) initializeRoutes() {

	// Home Route
	app.Router.GET("/", wrapHandlerFunc(app.Home))
	
	// Packet Route
	app.Router.POST("/packet", app.NewParserWorkerPoolFunc(100))

	// TslotAllocReq Route
	app.Router.POST("/talloc", app.NewTslotAllocWorkerPoolFunc(100))

	// Gateway admin routes
        app.Router.POST("/gateway", app.CreateGateway) // useful when there is no discovery service
        // TODO: these need to be implemented
        //app.Router.POST("/network", atc.CreateNetwork) // useful when there is no discovery service
        //app.Router.GET("/gateway", atc.GetGateways)
        //app.Router.GET("/gateway/:id", atc.GetGateway) <-- dont think this is doable with httprouter
        //app.Router.GET("/network", atc.GetNetworks)
        //app.Router.GET("/network/:id", atc.GetNetwork) <-- dont think this is doable with httprouter

	// TODO:
	// Add /ping
	// Add /health
	// Add version, lastboot, ... via status. See code in temp


}


// Wrapper function to make http handlers (i.e. goriall/mux) work with httprouter (i.e. julienschmidt)
// gorilla/context may be useful in the future if other http handlers (besides home) need to be converted to httprouter.
// source: https://www.nicolasmerouze.com/guide-routers-golang 
func wrapHandlerFunc(h http.HandlerFunc) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		context.Set(r, "params", ps)
		h.ServeHTTP(w, r)
	}
}
