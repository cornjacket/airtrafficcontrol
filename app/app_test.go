package app

import (

        "fmt"
	"testing"
	"log"
	"time"
	. "gopkg.in/check.v1"

	"bitbucket.org/cornjacket/airtrafficcontrol/app/controllers"
	"bitbucket.org/cornjacket/airtrafficcontrol/app/mocks"
	"bitbucket.org/cornjacket/iot/message"
	atc_client "bitbucket.org/cornjacket/airtrafficcontrol/client"
)

var client atc_client.AirTrafficControlService

func NewAppServiceMock() AppService {
        a := AppService{}
        a.Context = NewAppContextMock()
        return a
}

func NewAppContextMock() *controllers.AppContext {
        context := controllers.AppContext{}
	context.TallocRespService = &mocks.TallocRespServiceMock{} //&services.TallocRespService{} // implements ViewHandlrPostInterface
        return &context
}

// Hook up gocheck into the "go test" runner.
func Test(t *testing.T) { TestingT(t) }

type TestSuite struct{
	AppService	AppService
}

var _ = Suite(&TestSuite{})

func (s *TestSuite) SetUpSuite(c *C) {
	fmt.Println("SetUpSuite() invoked")
	s.AppService = NewAppServiceMock()
        s.AppService.InitEnv()
        s.AppService.DropTables()
        s.AppService.SetupDatabase()
        go s.AppService.Run()
	time.Sleep(1 * time.Second) // time delay to let server boot and talk to database
	if err := client.Open("localhost", s.AppService.Env.PortNum, "/packet", "/talloc"); err != nil {
		log.Fatal("client.Open() failed")
	}
}

func (s *TestSuite) SetUpTest(c *C) {
	fmt.Println("SetupTest() invoked")
	s.AppService.CreateTables()
	if err := s.AppService.Context.TallocRespService.Open("localhost", "8081", "/tallocResp.4.3.6"); err != nil {
		log.Fatal("TallocRespService.Open() failed")
	}
}

func (s *TestSuite) TearDownTest(c *C) {
	fmt.Println("TearDownTest() invoked")
        s.AppService.DropTables()
}

func (s *TestSuite) TearDownSuite(c *C) {
	fmt.Println("TearDownSuite() invoked")
	// TODO(drt) - close the db connection
}

// todo(drt) - atc receives rx packet instead of gw packet. what should atc response be?

// todo(drt): Add the gateway string and add to this interface
func sendOnboardGwPacket(c *C, eui string, gw_eui string) {
	// Send Packet
	fmt.Printf("sendOnboardPacket\n")

	gw := message.GatewayDescriptor{
        	Rssi:	-110,
        	Snr:	12.4,
        	Ts:	uint64(12345),
        	GwEui:	gw_eui, 
        	Lat:	123.12,
        	Lon:	43.32,
		}

	p := message.UpPacket{
        	Dr:	"?", 
        	Eui:	eui,
        	Ack: 	false,
        	Cmd:	"gw",
        	Data:	"FF00060004ffff00030000", // this is an onboard packet
        	Fcnt:	12,
        	Freq:	uint64(20),
        	Port:	1,
		Gws:	[]message.GatewayDescriptor{gw},
		}
	err := client.SendPacket(p)
	c.Assert(err, Equals, nil)
	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the event_hndlr is finished processing

}

func (s *TestSuite) TestOnboardPacketPath(c *C) {
	eui := "1234"
	gw_eui := "5678"
	sendOnboardGwPacket(c, eui, gw_eui)
}	

// Test out case where TallocReq comes before the Gw Packet
func (s *TestSuite) TestTallocReqErrorScenario(c *C) {
	eui := "1234"

	req := atc_client.TAllocReq{
        	NodeEui:	eui,
        	AppNum:		4,
        	AppApi:		3,
        	SysApi:		6,
        	Ts:		uint64(1234),
        	TimeReq:	15,
        	CorrId:		uint64(2),
        	ClassId:	"4.3.6",
        	Hops:		1,
		}

	err := client.RequestTslot(&req) 
	c.Assert(err, Equals, nil)
	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the event_hndlr is finished processing
	
	c.Assert(mocks.AtcLastTallocResp.NodeEui, Equals, eui)
	c.Assert(mocks.AtcLastTallocResp.Tslot, Equals, 0)
	c.Assert(mocks.AtcLastTallocResp.Code, Equals, -3) // todo(drt): define constants for code
}

// This tests an error scenario where atc recieves an rx packet.
func (s *TestSuite) TestXRxPacketPathErrorCase(c *C) {
	// Send Packet
	fmt.Printf("*****************TestRxPacketPathErrorCase\n")

	p := message.UpPacket{
        	Dr:	"?", 
        	Ts:	uint64(12345),
        	Eui:	"1234",
        	Ack: 	false,
        	Cmd:	"rx",
        	Snr:	12.4,
        	Data:	"12345678",
        	Fcnt:	12,
        	Freq:	uint64(20),
        	Port:	1,
        	Rssi:	-110,
		}
	err := client.SendPacket(p)
	c.Assert(err, Equals, nil)
	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the event_hndlr is finished processing

	// todo(DRT): atc should generate an error upon receiving an rx packet. need to build an external error service and then later an error service mock
	//		then this test can be modified to confirm that atc sends an alert to the error/notification service.
	// mock should still have default values as tallocresp service should not be invoked.
	c.Assert(mocks.AtcLastTallocResp.NodeEui, Equals, "") 
	c.Assert(mocks.AtcLastTallocResp.Tslot, Equals, 0)
	c.Assert(mocks.AtcLastTallocResp.Code, Equals, 0) // todo(drt): define constants for code

}

// Test out case where TallocReq comes after the Gw Packet
func (s *TestSuite) TestTallocReqScenario(c *C) {
	fmt.Printf("*****************TestTallocReqScenario\n")
	eui := "1234"
	gw_eui := "5678"
	sendOnboardGwPacket(c, eui, gw_eui)

	req := atc_client.TAllocReq{
        	NodeEui:	eui,
        	AppNum:		4,
        	AppApi:		3,
        	SysApi:		6,
        	Ts:		uint64(1234),
        	TimeReq:	15,
        	CorrId:		uint64(2),
        	ClassId:	"4.3.6",
        	Hops:		1,
		}

	err := client.RequestTslot(&req) 
	c.Assert(err, Equals, nil)
	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the event_hndlr is finished processing

	c.Assert(mocks.AtcLastTallocResp.NodeEui, Equals, eui)
	c.Assert(mocks.AtcLastTallocResp.Tslot, Equals, 2)
	c.Assert(mocks.AtcLastTallocResp.Code, Equals, 1) // todo(drt): define constants for code

/* just for reference
type TallocResp struct {
        NodeEui         string
        Tslot           int
        Code            int // not sure how i will use this. maybe indicate whether valid or oversubscribed: >0 will be valid, otherwise error code
        Test            bool
        Ts              uint64
        CorrId  uint64  `json:"corrid"`
        ClassId string  `json:"classid"`
        Hops int        `json:"hops"`
}
*/
}

