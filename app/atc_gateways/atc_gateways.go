package atc_gateways

import (
  // Standard library packets
  "fmt"
  "os"
  "strconv"
  "database/sql"
  _ "github.com/go-sql-driver/mysql"
  "time"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_gateways/main/eui"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_gateways/main/validonboard"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_gateways/main/createdat"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_gateways/main/updatedat"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_gateways/main/tslottype"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_gateways/main/totaltslots"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_gateways/main/allocatedtslots"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_gateways/main/subband"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_gateways/main/networkid"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_gateways/main/simpleallocation"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_gateways/main/nextavailabletslot"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_gateways/main/maxnodespertslot"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_gateways/main/countincurrenttslot"
)

type Row struct {
  Eui	string	`json:"eui"`
  ValidOnboard	int	`json:"validonboard"`
  CreatedAt	time.Time	`json:"createdat"`
  UpdatedAt	time.Time	`json:"updatedat"`
  TslotType	int	`json:"tslottype"`
  TotalTslots	int	`json:"totaltslots"`
  AllocatedTslots	int	`json:"allocatedtslots"`
  Subband	int	`json:"subband"`
  NetworkId	int	`json:"networkid"`
  SimpleAllocation	int	`json:"simpleallocation"`
  NextAvailableTslot	int	`json:"nextavailabletslot"`
  MaxNodesPerTslot	int	`json:"maxnodespertslot"`
  CountInCurrentTslot	int	`json:"countincurrenttslot"`
}

type PrevRow struct {
  Eui	string	`json:"eui"`
  ValidOnboard	int	`json:"validonboard"`
  CreatedAt	time.Time	`json:"createdat"`
  UpdatedAt	time.Time	`json:"updatedat"`
  TslotType	int	`json:"tslottype"`
  TotalTslots	int	`json:"totaltslots"`
  AllocatedTslots	int	`json:"allocatedtslots"`
  Subband	int	`json:"subband"`
  NetworkId	int	`json:"networkid"`
  SimpleAllocation	int	`json:"simpleallocation"`
  NextAvailableTslot	int	`json:"nextavailabletslot"`
  MaxNodesPerTslot	int	`json:"maxnodespertslot"`
  CountInCurrentTslot	int	`json:"countincurrenttslot"`
}

var db_conn *sql.DB

func Init(db *sql.DB) {
 fmt.Printf("Atc_Gateways.Init() invoked\n")
  if db != nil {
    db_conn = db
  } else {
   fmt.Printf("Atc_Gateways.Init() db = nil. Exiting...\n")
    os.Exit(0)
  }
  atc_gateways := New()
  //currentTable := atc_gateways.GetCurrentTableName()
  //fmt.Printf("currentTable: %s, CreateTableName: %s\n", currentTable, atc_gateways.CreateTableString())
  // Create table if it doesn't already exist
  err := atc_gateways.CreateTable()
  if err != nil {
    fmt.Printf("atc_gateways.CreateTable() failed. Assuming already exists.\n")
  } else {
    fmt.Printf("atc_gateways.CreateTable() succeeded!!\n")
  }
}

type Atc_Gateways struct {
  db           *sql.DB
  tx           *sql.Tx // used for the transaction
  txValid      bool // determines whether a transaction is currently in progress
  tableName    string
  prevVersion  int
  version      int
  constraints  string
  Eui	*eui.Eui
  ValidOnboard	*validonboard.ValidOnboard
  CreatedAt	*createdat.CreatedAt
  UpdatedAt	*updatedat.UpdatedAt
  TslotType	*tslottype.TslotType
  TotalTslots	*totaltslots.TotalTslots
  AllocatedTslots	*allocatedtslots.AllocatedTslots
  Subband	*subband.Subband
  NetworkId	*networkid.NetworkId
  SimpleAllocation	*simpleallocation.SimpleAllocation
  NextAvailableTslot	*nextavailabletslot.NextAvailableTslot
  MaxNodesPerTslot	*maxnodespertslot.MaxNodesPerTslot
  CountInCurrentTslot	*countincurrenttslot.CountInCurrentTslot
}

func New() *Atc_Gateways {
  x := &Atc_Gateways{}
  x.db = db_conn
  x.txValid = false
  x.tableName = "atc_gateways"
  x.prevVersion = 3
  x.version = 4
  x.Eui=	eui.New()
  x.ValidOnboard=	validonboard.New()
  x.CreatedAt=	createdat.New()
  x.UpdatedAt=	updatedat.New()
  x.TslotType=	tslottype.New()
  x.TotalTslots=	totaltslots.New()
  x.AllocatedTslots=	allocatedtslots.New()
  x.Subband=	subband.New()
  x.NetworkId=	networkid.New()
  x.SimpleAllocation=	simpleallocation.New()
  x.NextAvailableTslot=	nextavailabletslot.New()
  x.MaxNodesPerTslot=	maxnodespertslot.New()
  x.CountInCurrentTslot=	countincurrenttslot.New()
  x.constraints = "constraint pk_example primary key (eui)"
  return x
}

var prevTableExists bool = false // TODO: Create Mechanism to auto detect if prevTable exists

func PrevTableExists() bool {
  return prevTableExists;
}

// Close() should be called after all DB operations are done. If a transaction is currently in progress, then Close will Rollback or Commit based on the DB error
// sent to Close(). If Close or Rollback has already been called by the user, then calling Close() should have no effect.
func (x *Atc_Gateways) Close (err error) (error, bool) {
  if x.txValid {
    if err != nil {
      return x.tx.Rollback(), true
    } else {
      return x.tx.Commit(), true
    }
  }
  return nil, false
}

// If transaction is in progress, then turn off the transaction state and call rollback, releasing the lock
func (x *Atc_Gateways) Rollback () (error, bool) {
  if x.txValid {
    x.txValid = false
    return x.tx.Rollback(), true
  }
  return nil, false
}

// If transaction is in progress, then turn off the transaction state and call commit, releasing the lock
func (x *Atc_Gateways) Commit() (error, bool) {
  if x.txValid {
    x.txValid = false
    return x.tx.Commit(), true
  }
  return nil, false
}

func (x *Atc_Gateways) GetByEui (eui string, createIfNotFound bool, lockRow bool) (error, bool) {
  //fmt.Printf("table.GetByEui(): Entrance.\n")
  x.Eui.Load(eui)  // required: all rows must have a valid_onboard field that defaults to false, used to indicate if a field was created properly.
  // getByEui should select/search for entry in current version table
  err, rcvd := x.SelectFromCurrent(eui, lockRow)
  // if entryfound then process_normally
  if err == nil && rcvd == true {
    //fmt.Printf("table.GetByEui(): Returning nil, no error, rcvd one entry..\n")
    return nil, true
  }
  // else // no entry found
  //fmt.Printf("table.GetByEui(): row DNE in current table.\n")
  if createIfNotFound == true {
    if PrevTableExists() {
      //fmt.Printf("table.GetByEui(): checking prev table\n")
      err, rcvd = x.SelectFromPrev(eui)
      entryFound := (err == nil && rcvd == true)
      if entryFound {
//       copy over parameters from old row to new row, along with new row's new default fields, with valid_onboard set to true
        x.HandleMigrationOrRollback()
        err = x.insert()
        if err != nil {
          //fmt.Printf("table.GetByEui(): x.insert failed.\n")
          return err, rcvd
        }
        //fmt.Printf("table.GetByEui(): x.insert succeeded.\n")
        err = x.DeleteByEui(x.GetPrevTableName())
        if err != nil {
          //fmt.Printf("table.GetByEui(): x.delete %s failed from PrevTable.\n")
          return err, rcvd
        }
      } else {
//       create default row with valid_onboard = false -- How to do this?
        err = x.insert() // later we can just return this i think
        if err != nil {
          //fmt.Printf("table.GetByEui(): x.insert failed.\n")
          return err, rcvd
        }
//       create default row with valid_onboard = false
      }
    } else { // later we can just return this i think
      err = x.insert()
      if err != nil {
        //fmt.Printf("table.GetByEui(): x.insert failed.\n")
        return err, rcvd
      }
    }
    // TODO: If lockRow, then SelectFromCurrent (with lock on) should be called again since now the row has been created... otherwise there is a potential race condition
  }
  return err, rcvd
}

func (x *Atc_Gateways) HandleMigrationOrRollback() {
// currently doing nothing but this is where the migration or rollback logic should happen
// this should be generated code based on whether migration or rollback
fmt.Printf("table.HandleColumnsForMigrationOrRollback(): \n")
}

func (x *Atc_Gateways) CreateTable() error {
  _, err := x.db.Exec(x.CreateTableString())
  return err
}

func (x *Atc_Gateways) DeleteByEui(tableName string) error {
  _, err := x.db.Exec(x.DeleteString(tableName))
  return err
}

func (x *Atc_Gateways) Insert(row *Row) error {
  x.Eui.Load(row.Eui)
  x.ValidOnboard.Load(row.ValidOnboard)
  x.TslotType.Load(row.TslotType)
  x.TotalTslots.Load(row.TotalTslots)
  x.AllocatedTslots.Load(row.AllocatedTslots)
  x.Subband.Load(row.Subband)
  x.NetworkId.Load(row.NetworkId)
  x.SimpleAllocation.Load(row.SimpleAllocation)
  x.NextAvailableTslot.Load(row.NextAvailableTslot)
  x.MaxNodesPerTslot.Load(row.MaxNodesPerTslot)
  x.CountInCurrentTslot.Load(row.CountInCurrentTslot)
  return x.insert()
}

func (x *Atc_Gateways) insert() error {
  insertString := x.InsertString()
  //fmt.Printf("table.InsertString(): %s\n", insertString)
  var err error
  if x.txValid {
    _, err = x.tx.Exec(insertString,
     x.Eui.Get(),
     x.ValidOnboard.Get(),
     x.TslotType.Get(),
     x.TotalTslots.Get(),
     x.AllocatedTslots.Get(),
     x.Subband.Get(),
     x.NetworkId.Get(),
     x.SimpleAllocation.Get(),
     x.NextAvailableTslot.Get(),
     x.MaxNodesPerTslot.Get(),
     x.CountInCurrentTslot.Get(),
    )
  } else {
    _, err = x.db.Exec(insertString,
     x.Eui.Get(),
     x.ValidOnboard.Get(),
     x.TslotType.Get(),
     x.TotalTslots.Get(),
     x.AllocatedTslots.Get(),
     x.Subband.Get(),
     x.NetworkId.Get(),
     x.SimpleAllocation.Get(),
     x.NextAvailableTslot.Get(),
     x.MaxNodesPerTslot.Get(),
     x.CountInCurrentTslot.Get(),
    )
  }
  if err != nil {
    return err
  }
  return nil
}

// updated indicates whether any fields have changed and thus an update to the db was attempted 
func (x *Atc_Gateways) Update() (anyFieldModified bool, err error) {
  updateString, anyFieldModified := x.UpdateString()
  //fmt.Printf("table.UpdateString(): %s\n", updateString)

  if anyFieldModified {
    if x.txValid {
      _, err = x.tx.Exec(updateString)
    } else {
      _, err = x.db.Exec(updateString)
    }
  }
  if err != nil {
    return anyFieldModified, err
  }
  return anyFieldModified, nil
}

func (x *Atc_Gateways) SelectFromCurrent(eui string, lockRow bool) (error, bool) { // later this will be SelectByEui(eui string)
  //d.Id = id
  x.Eui.Load(eui)
  tableName := x.GetCurrentTableName()
  var err error
  var tx *sql.Tx
  x.txValid = lockRow
  selectString := x.SelectString(tableName) // we could pass eui into select for a nicer abstraction
  //fmt.Printf("table.SelectString(): %s\n", selectString)

  if x.txValid {
    tx, err = x.db.Begin()
    x.tx = tx
    if err != nil {
      //fmt.Printf("selectForCurrent x.db.Begin() failed\n")
      return err, false
    }
  }

  var results *sql.Rows
  if x.txValid {
    results, err = tx.Query(selectString)
  } else {
    results, err = x.db.Query(selectString)
  }
  if err != nil {
    //fmt.Printf("tx.Query 1 fail\n")
    return err, false
  } else {
    defer results.Close()

    //fmt.Printf("tx.Query 1 pass\n")
  }


  for results.Next() {
    var row Row
//err = results.Scan(&row.Id, &row.Eui, &row.SysApi, &row.AppApi)
    err = results.Scan( &row.Eui, &row.ValidOnboard, &row.CreatedAt, &row.UpdatedAt, &row.TslotType, &row.TotalTslots, &row.AllocatedTslots, &row.Subband, &row.NetworkId, &row.SimpleAllocation, &row.NextAvailableTslot, &row.MaxNodesPerTslot, &row.CountInCurrentTslot,)
  if err != nil {
  return err, false
  }
 x.Eui.Load(row.Eui)
 x.ValidOnboard.Load(row.ValidOnboard)
 x.CreatedAt.Load(row.CreatedAt)
 x.UpdatedAt.Load(row.UpdatedAt)
 x.TslotType.Load(row.TslotType)
 x.TotalTslots.Load(row.TotalTslots)
 x.AllocatedTslots.Load(row.AllocatedTslots)
 x.Subband.Load(row.Subband)
 x.NetworkId.Load(row.NetworkId)
 x.SimpleAllocation.Load(row.SimpleAllocation)
 x.NextAvailableTslot.Load(row.NextAvailableTslot)
 x.MaxNodesPerTslot.Load(row.MaxNodesPerTslot)
 x.CountInCurrentTslot.Load(row.CountInCurrentTslot)
    return nil, true
  }
  return nil, false
}

func (x *Atc_Gateways) GetPrevTableName() string {
  return tableName(x.tableName, x.prevVersion)
}

//func (x *Atc_Gateways) Select(id int64) error { // later this will be SelectByEui(eui string)
func (x *Atc_Gateways) SelectFromPrev(eui string) (error, bool)  { // later this will be SelectByEui(eui string)
  //x.Id = id
  x.Eui.Load(eui)
  prevTable := x.GetPrevTableName()
  selectString := x.SelectString(prevTable) // we could pass eui into select for a nicer abstraction
  //fmt.Printf("table.SelectString(): %s\n", selectString)
  results, err := x.db.Query(selectString)
  if err != nil {
    return err, false
  }
  defer results.Close()
  for results.Next() {
    var row PrevRow
    err = results.Scan( &row.Eui, &row.ValidOnboard, &row.CreatedAt, &row.UpdatedAt, &row.TslotType, &row.TotalTslots, &row.AllocatedTslots, &row.Subband, &row.NetworkId, &row.SimpleAllocation, &row.NextAvailableTslot, &row.MaxNodesPerTslot, &row.CountInCurrentTslot,)
    if err != nil {
       return err, false
    }
    x.Eui.Load(row.Eui)
    x.ValidOnboard.Load(row.ValidOnboard)
    x.CreatedAt.Load(row.CreatedAt)
    x.UpdatedAt.Load(row.UpdatedAt)
    x.TslotType.Load(row.TslotType)
    x.TotalTslots.Load(row.TotalTslots)
    x.AllocatedTslots.Load(row.AllocatedTslots)
    x.Subband.Load(row.Subband)
    x.NetworkId.Load(row.NetworkId)
    x.SimpleAllocation.Load(row.SimpleAllocation)
    x.NextAvailableTslot.Load(row.NextAvailableTslot)
    x.MaxNodesPerTslot.Load(row.MaxNodesPerTslot)
    x.CountInCurrentTslot.Load(row.CountInCurrentTslot)
    return nil, true
  }
  return nil, false
}

func (x *Atc_Gateways) CreateTableString() string {
  return "create table " + x.GetCurrentTableName() +
  " ( " + x.Eui.String() + " " + x.Eui.Type() + " " + x.Eui.Modifiers() +
  ", " + x.ValidOnboard.String() + " " + x.ValidOnboard.Type() + " " + x.ValidOnboard.Modifiers() +
  ", " + x.CreatedAt.String() + " " + x.CreatedAt.Type() + " " + x.CreatedAt.Modifiers() +
  ", " + x.UpdatedAt.String() + " " + x.UpdatedAt.Type() + " " + x.UpdatedAt.Modifiers() +
  ", " + x.TslotType.String() + " " + x.TslotType.Type() + " " + x.TslotType.Modifiers() +
  ", " + x.TotalTslots.String() + " " + x.TotalTslots.Type() + " " + x.TotalTslots.Modifiers() +
  ", " + x.AllocatedTslots.String() + " " + x.AllocatedTslots.Type() + " " + x.AllocatedTslots.Modifiers() +
  ", " + x.Subband.String() + " " + x.Subband.Type() + " " + x.Subband.Modifiers() +
  ", " + x.NetworkId.String() + " " + x.NetworkId.Type() + " " + x.NetworkId.Modifiers() +
  ", " + x.SimpleAllocation.String() + " " + x.SimpleAllocation.Type() + " " + x.SimpleAllocation.Modifiers() +
  ", " + x.NextAvailableTslot.String() + " " + x.NextAvailableTslot.Type() + " " + x.NextAvailableTslot.Modifiers() +
  ", " + x.MaxNodesPerTslot.String() + " " + x.MaxNodesPerTslot.Type() + " " + x.MaxNodesPerTslot.Modifiers() +
  ", " + x.CountInCurrentTslot.String() + " " + x.CountInCurrentTslot.Type() + " " + x.CountInCurrentTslot.Modifiers() +
  ", " + x.constraints + " );"
}

// used internally for creating table name with current or previous version
func tableName(name string, version int) string {
   return name + "_v" + strconv.Itoa(version)
}

func newDeployment(current_version int, previous_version int) bool {
  if current_version == (previous_version + 1) {
    return true;
  }
  return false;
}

func (x *Atc_Gateways) GetCurrentTableName() string {
  return tableName(x.tableName, x.version)
}

func (x *Atc_Gateways) SelectString(tableName string) string {
  if x.txValid {
    return "select * from " + tableName + " where eui = " + x.Eui.QueryValue() + " for update;"
  } else {
    return "select * from " + tableName + " where eui = " + x.Eui.QueryValue() + ";"
  }
}

// insert is only done to the current table
func (x *Atc_Gateways) InsertString() string {
  return "insert into " + x.GetCurrentTableName() +
   " (" + x.Eui.String() + " , " +  x.ValidOnboard.String() + " , " +  x.TslotType.String() + " , " +  x.TotalTslots.String() + " , " +  x.AllocatedTslots.String() + " , " +  x.Subband.String() + " , " +  x.NetworkId.String() + " , " +  x.SimpleAllocation.String() + " , " +  x.NextAvailableTslot.String() + " , " +  x.MaxNodesPerTslot.String() + " , " +  x.CountInCurrentTslot.String() + ") values" +
   " (?"+ " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + ");"
}

// function returns true if at least one field has been updated
func (x *Atc_Gateways) UpdateString() (string, bool) {
  count := 0
  first := true
  return_str := "update " + x.GetCurrentTableName() + " set "
  if x.ValidOnboard.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ValidOnboard.String() + " = " + x.ValidOnboard.QueryValue()
    first = false
  }
  if x.TslotType.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.TslotType.String() + " = " + x.TslotType.QueryValue()
    first = false
  }
  if x.TotalTslots.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.TotalTslots.String() + " = " + x.TotalTslots.QueryValue()
    first = false
  }
  if x.AllocatedTslots.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.AllocatedTslots.String() + " = " + x.AllocatedTslots.QueryValue()
    first = false
  }
  if x.Subband.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Subband.String() + " = " + x.Subband.QueryValue()
    first = false
  }
  if x.NetworkId.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.NetworkId.String() + " = " + x.NetworkId.QueryValue()
    first = false
  }
  if x.SimpleAllocation.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.SimpleAllocation.String() + " = " + x.SimpleAllocation.QueryValue()
    first = false
  }
  if x.NextAvailableTslot.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.NextAvailableTslot.String() + " = " + x.NextAvailableTslot.QueryValue()
    first = false
  }
  if x.MaxNodesPerTslot.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.MaxNodesPerTslot.String() + " = " + x.MaxNodesPerTslot.QueryValue()
    first = false
  }
  if x.CountInCurrentTslot.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.CountInCurrentTslot.String() + " = " + x.CountInCurrentTslot.QueryValue()
    first = false
  }
  return_str += " where Eui = " + x.Eui.QueryValue() + ";"
  if count > 0 {
    return return_str, true
  } else {
    return "", false
  }
}

func (x *Atc_Gateways) DeleteString(tableName string) string {
  return "delete from " + tableName + " where eui = " + x.Eui.QueryValue() + ";"
}

