package tslottype

import (
  "strconv"
)

type TslotType struct {
  value int
  updated bool
}

func New() *TslotType {
  return &TslotType{}
}

func (x *TslotType) IsUpdated() bool {
  return x.updated
}

func (x *TslotType) Get() int {
  return x.value
}

func (x *TslotType) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *TslotType) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *TslotType) String() string {
  return "TslotType"
}

func (x *TslotType) Type() string {
  return "int"
}

func (x *TslotType) Modifiers() string {
  return "not null"
}

func (x *TslotType) QueryValue() string {
  return strconv.Itoa(x.value)
}

