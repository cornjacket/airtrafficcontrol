package allocatedtslots

import (
  "strconv"
)

type AllocatedTslots struct {
  value int
  updated bool
}

func New() *AllocatedTslots {
  return &AllocatedTslots{}
}

func (x *AllocatedTslots) IsUpdated() bool {
  return x.updated
}

func (x *AllocatedTslots) Get() int {
  return x.value
}

func (x *AllocatedTslots) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *AllocatedTslots) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *AllocatedTslots) String() string {
  return "AllocatedTslots"
}

func (x *AllocatedTslots) Type() string {
  return "int"
}

func (x *AllocatedTslots) Modifiers() string {
  return "not null"
}

func (x *AllocatedTslots) QueryValue() string {
  return strconv.Itoa(x.value)
}

