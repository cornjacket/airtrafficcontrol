package totaltslots

import (
  "strconv"
)

type TotalTslots struct {
  value int
  updated bool
}

func New() *TotalTslots {
  return &TotalTslots{}
}

func (x *TotalTslots) IsUpdated() bool {
  return x.updated
}

func (x *TotalTslots) Get() int {
  return x.value
}

func (x *TotalTslots) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *TotalTslots) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *TotalTslots) String() string {
  return "TotalTslots"
}

func (x *TotalTslots) Type() string {
  return "int"
}

func (x *TotalTslots) Modifiers() string {
  return "not null"
}

func (x *TotalTslots) QueryValue() string {
  return strconv.Itoa(x.value)
}

