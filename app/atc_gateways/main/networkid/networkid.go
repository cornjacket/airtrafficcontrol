package networkid

import (
  "strconv"
)

type NetworkId struct {
  value int
  updated bool
}

func New() *NetworkId {
  return &NetworkId{}
}

func (x *NetworkId) IsUpdated() bool {
  return x.updated
}

func (x *NetworkId) Get() int {
  return x.value
}

func (x *NetworkId) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *NetworkId) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *NetworkId) String() string {
  return "NetworkId"
}

func (x *NetworkId) Type() string {
  return "int"
}

func (x *NetworkId) Modifiers() string {
  return "not null"
}

func (x *NetworkId) QueryValue() string {
  return strconv.Itoa(x.value)
}

