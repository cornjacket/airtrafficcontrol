package maxnodespertslot

import (
  "strconv"
)

type MaxNodesPerTslot struct {
  value int
  updated bool
}

func New() *MaxNodesPerTslot {
  return &MaxNodesPerTslot{}
}

func (x *MaxNodesPerTslot) IsUpdated() bool {
  return x.updated
}

func (x *MaxNodesPerTslot) Get() int {
  return x.value
}

func (x *MaxNodesPerTslot) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *MaxNodesPerTslot) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *MaxNodesPerTslot) String() string {
  return "MaxNodesPerTslot"
}

func (x *MaxNodesPerTslot) Type() string {
  return "int"
}

func (x *MaxNodesPerTslot) Modifiers() string {
  return "not null"
}

func (x *MaxNodesPerTslot) QueryValue() string {
  return strconv.Itoa(x.value)
}

