package nextavailabletslot

import (
  "strconv"
)

type NextAvailableTslot struct {
  value int
  updated bool
}

func New() *NextAvailableTslot {
  return &NextAvailableTslot{}
}

func (x *NextAvailableTslot) IsUpdated() bool {
  return x.updated
}

func (x *NextAvailableTslot) Get() int {
  return x.value
}

func (x *NextAvailableTslot) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *NextAvailableTslot) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *NextAvailableTslot) String() string {
  return "NextAvailableTslot"
}

func (x *NextAvailableTslot) Type() string {
  return "int"
}

func (x *NextAvailableTslot) Modifiers() string {
  return "not null"
}

func (x *NextAvailableTslot) QueryValue() string {
  return strconv.Itoa(x.value)
}

