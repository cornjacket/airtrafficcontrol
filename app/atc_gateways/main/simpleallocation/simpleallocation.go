package simpleallocation

import (
  "strconv"
)

type SimpleAllocation struct {
  value int
  updated bool
}

func New() *SimpleAllocation {
  return &SimpleAllocation{}
}

func (x *SimpleAllocation) IsUpdated() bool {
  return x.updated
}

func (x *SimpleAllocation) Get() int {
  return x.value
}

func (x *SimpleAllocation) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *SimpleAllocation) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *SimpleAllocation) String() string {
  return "SimpleAllocation"
}

func (x *SimpleAllocation) Type() string {
  return "int"
}

func (x *SimpleAllocation) Modifiers() string {
  return "not null"
}

func (x *SimpleAllocation) QueryValue() string {
  return strconv.Itoa(x.value)
}

