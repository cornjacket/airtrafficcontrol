package countincurrenttslot

import (
  "strconv"
)

type CountInCurrentTslot struct {
  value int
  updated bool
}

func New() *CountInCurrentTslot {
  return &CountInCurrentTslot{}
}

func (x *CountInCurrentTslot) IsUpdated() bool {
  return x.updated
}

func (x *CountInCurrentTslot) Get() int {
  return x.value
}

func (x *CountInCurrentTslot) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *CountInCurrentTslot) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *CountInCurrentTslot) String() string {
  return "CountInCurrentTslot"
}

func (x *CountInCurrentTslot) Type() string {
  return "int"
}

func (x *CountInCurrentTslot) Modifiers() string {
  return "not null"
}

func (x *CountInCurrentTslot) QueryValue() string {
  return strconv.Itoa(x.value)
}

