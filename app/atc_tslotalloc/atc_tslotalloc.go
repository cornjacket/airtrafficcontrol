package atc_tslotalloc

import (
  // Standard library packets
  "fmt"
  "os"
  "strconv"
  "database/sql"
  _ "github.com/go-sql-driver/mysql"
  "time"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_tslotalloc/main/id"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_tslotalloc/main/eui"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_tslotalloc/main/createdat"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_tslotalloc/main/updatedat"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_tslotalloc/main/gatewayeui"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_tslotalloc/main/tslot"
)

type Row struct {
  Id	int64	`json:"id"`
  Eui	string	`json:"eui"`
  CreatedAt	time.Time	`json:"createdat"`
  UpdatedAt	time.Time	`json:"updatedat"`
  GatewayEui	string	`json:"gatewayeui"`
  Tslot	int	`json:"tslot"`
}

type PrevRow struct {
  Id	int64	`json:"id"`
  Eui	string	`json:"eui"`
  CreatedAt	time.Time	`json:"createdat"`
  UpdatedAt	time.Time	`json:"updatedat"`
  GatewayEui	string	`json:"gatewayeui"`
  Tslot	int	`json:"tslot"`
}

var db_conn *sql.DB

func Init(db *sql.DB) {
 fmt.Printf("Atc_TslotAlloc.Init() invoked\n")
  if db != nil {
    db_conn = db
  } else {
   fmt.Printf("Atc_TslotAlloc.Init() db = nil. Exiting...\n")
    os.Exit(0)
  }
  atc_tslotalloc := New()
  //currentTable := atc_tslotalloc.GetCurrentTableName()
  //fmt.Printf("currentTable: %s, CreateTableName: %s\n", currentTable, atc_tslotalloc.CreateTableString())
  // Create table if it doesn't already exist
  err := atc_tslotalloc.CreateTable()
  if err != nil {
    fmt.Printf("atc_tslotalloc.CreateTable() failed. Assuming already exists.\n")
  } else {
    fmt.Printf("atc_tslotalloc.CreateTable() succeeded!!\n")
  }
}

type Atc_TslotAlloc struct {
  db           *sql.DB
  tx           *sql.Tx // used for the transaction
  txValid      bool // determines whether a transaction is currently in progress
  tableName    string
  prevVersion  int
  version      int
  constraints  string
  Id	*id.Id
  Eui	*eui.Eui
  CreatedAt	*createdat.CreatedAt
  UpdatedAt	*updatedat.UpdatedAt
  GatewayEui	*gatewayeui.GatewayEui
  Tslot	*tslot.Tslot
}

func New() *Atc_TslotAlloc {
  x := &Atc_TslotAlloc{}
  x.db = db_conn
  x.txValid = false
  x.tableName = "atc_tslotalloc"
  x.prevVersion = 1
  x.version = 2
  x.Id=	id.New()
  x.Eui=	eui.New()
  x.CreatedAt=	createdat.New()
  x.UpdatedAt=	updatedat.New()
  x.GatewayEui=	gatewayeui.New()
  x.Tslot=	tslot.New()
  x.constraints = "constraint pk_example primary key (id)"
  return x
}

var prevTableExists bool = false // TODO: Create Mechanism to auto detect if prevTable exists

func PrevTableExists() bool {
  return prevTableExists;
}

// Close() should be called after all DB operations are done. If a transaction is currently in progress, then Close will Rollback or Commit based on the DB error
// sent to Close(). If Close or Rollback has already been called by the user, then calling Close() should have no effect.
func (x *Atc_TslotAlloc) Close (err error) (error, bool) {
  if x.txValid {
    if err != nil {
      return x.tx.Rollback(), true
    } else {
      return x.tx.Commit(), true
    }
  }
  return nil, false
}

// If transaction is in progress, then turn off the transaction state and call rollback, releasing the lock
func (x *Atc_TslotAlloc) Rollback () (error, bool) {
  if x.txValid {
    x.txValid = false
    return x.tx.Rollback(), true
  }
  return nil, false
}

// If transaction is in progress, then turn off the transaction state and call commit, releasing the lock
func (x *Atc_TslotAlloc) Commit() (error, bool) {
  if x.txValid {
    x.txValid = false
    return x.tx.Commit(), true
  }
  return nil, false
}

func (x *Atc_TslotAlloc) GetByEui (eui string, createIfNotFound bool, lockRow bool) (error, bool) {
  //fmt.Printf("table.GetByEui(): Entrance.\n")
  x.Eui.Load(eui)  // required: all rows must have a valid_onboard field that defaults to false, used to indicate if a field was created properly.
  // getByEui should select/search for entry in current version table
  err, rcvd := x.SelectFromCurrent(eui, lockRow)
  // if entryfound then process_normally
  if err == nil && rcvd == true {
    //fmt.Printf("table.GetByEui(): Returning nil, no error, rcvd one entry..\n")
    return nil, true
  }
  // else // no entry found
  //fmt.Printf("table.GetByEui(): row DNE in current table.\n")
  if createIfNotFound == true {
    if PrevTableExists() {
      //fmt.Printf("table.GetByEui(): checking prev table\n")
      err, rcvd = x.SelectFromPrev(eui)
      entryFound := (err == nil && rcvd == true)
      if entryFound {
//       copy over parameters from old row to new row, along with new row's new default fields, with valid_onboard set to true
        x.HandleMigrationOrRollback()
        err = x.insert()
        if err != nil {
          //fmt.Printf("table.GetByEui(): x.insert failed.\n")
          return err, rcvd
        }
        //fmt.Printf("table.GetByEui(): x.insert succeeded.\n")
        err = x.DeleteByEui(x.GetPrevTableName())
        if err != nil {
          //fmt.Printf("table.GetByEui(): x.delete %s failed from PrevTable.\n")
          return err, rcvd
        }
      } else {
//       create default row with valid_onboard = false -- How to do this?
        err = x.insert() // later we can just return this i think
        if err != nil {
          //fmt.Printf("table.GetByEui(): x.insert failed.\n")
          return err, rcvd
        }
//       create default row with valid_onboard = false
      }
    } else { // later we can just return this i think
      err = x.insert()
      if err != nil {
        //fmt.Printf("table.GetByEui(): x.insert failed.\n")
        return err, rcvd
      }
    }
    // TODO: If lockRow, then SelectFromCurrent (with lock on) should be called again since now the row has been created... otherwise there is a potential race condition
  }
  return err, rcvd
}

func (x *Atc_TslotAlloc) HandleMigrationOrRollback() {
// currently doing nothing but this is where the migration or rollback logic should happen
// this should be generated code based on whether migration or rollback
fmt.Printf("table.HandleColumnsForMigrationOrRollback(): \n")
}

func (x *Atc_TslotAlloc) CreateTable() error {
  _, err := x.db.Exec(x.CreateTableString())
  return err
}

func (x *Atc_TslotAlloc) DeleteByEui(tableName string) error {
  _, err := x.db.Exec(x.DeleteString(tableName))
  return err
}

func (x *Atc_TslotAlloc) Insert(row *Row) error {
  x.Eui.Load(row.Eui)
  x.GatewayEui.Load(row.GatewayEui)
  x.Tslot.Load(row.Tslot)
  return x.insert()
}

func (x *Atc_TslotAlloc) insert() error {
  insertString := x.InsertString()
  //fmt.Printf("table.InsertString(): %s\n", insertString)
  var insert sql.Result 
  var err error
  // insert is now not need with Eui
  if x.txValid {
     insert, err = x.tx.Exec(insertString,
     x.Eui.Get(),
     x.GatewayEui.Get(),
     x.Tslot.Get(),
    )
  } else {
     insert, err = x.db.Exec(insertString,
     x.Eui.Get(),
     x.GatewayEui.Get(),
     x.Tslot.Get(),
    )
  }
  if err != nil {
    return err
  }
  Id, err := insert.LastInsertId() // Id is not defined in EUI context, though it could be useful in other contexts
  if err != nil {
    return err
  }
  x.Id.Load(Id)
  //fmt.Printf("Insert id: %d\n", x.Id)
  return nil
}

// updated indicates whether any fields have changed and thus an update to the db was attempted 
func (x *Atc_TslotAlloc) Update() (anyFieldModified bool, err error) {
  updateString, anyFieldModified := x.UpdateString()
  //fmt.Printf("table.UpdateString(): %s\n", updateString)

  if anyFieldModified {
    if x.txValid {
      _, err = x.tx.Exec(updateString)
    } else {
      _, err = x.db.Exec(updateString)
    }
  }
  if err != nil {
    return anyFieldModified, err
  }
  return anyFieldModified, nil
}

func (x *Atc_TslotAlloc) SelectFromCurrent(eui string, lockRow bool) (error, bool) { // later this will be SelectByEui(eui string)
  //d.Id = id
  x.Eui.Load(eui)
  tableName := x.GetCurrentTableName()
  var err error
  var tx *sql.Tx
  x.txValid = lockRow
  selectString := x.SelectString(tableName) // we could pass eui into select for a nicer abstraction
  //fmt.Printf("table.SelectString(): %s\n", selectString)

  if x.txValid {
    tx, err = x.db.Begin()
    x.tx = tx
    if err != nil {
      //fmt.Printf("selectForCurrent x.db.Begin() failed\n")
      return err, false
    }
  }

  var results *sql.Rows
  if x.txValid {
    results, err = tx.Query(selectString)
  } else {
    results, err = x.db.Query(selectString)
  }
  if err != nil {
    //fmt.Printf("tx.Query 1 fail\n")
    return err, false
  } else {
    defer results.Close()

    //fmt.Printf("tx.Query 1 pass\n")
  }


  for results.Next() {
    var row Row
//err = results.Scan(&row.Id, &row.Eui, &row.SysApi, &row.AppApi)
    err = results.Scan( &row.Id, &row.Eui, &row.CreatedAt, &row.UpdatedAt, &row.GatewayEui, &row.Tslot,)
  if err != nil {
  return err, false
  }
 x.Id.Load(row.Id)
 x.Eui.Load(row.Eui)
 x.CreatedAt.Load(row.CreatedAt)
 x.UpdatedAt.Load(row.UpdatedAt)
 x.GatewayEui.Load(row.GatewayEui)
 x.Tslot.Load(row.Tslot)
    return nil, true
  }
  return nil, false
}

func (x *Atc_TslotAlloc) GetPrevTableName() string {
  return tableName(x.tableName, x.prevVersion)
}

//func (x *Atc_TslotAlloc) Select(id int64) error { // later this will be SelectByEui(eui string)
func (x *Atc_TslotAlloc) SelectFromPrev(eui string) (error, bool)  { // later this will be SelectByEui(eui string)
  //x.Id = id
  x.Eui.Load(eui)
  prevTable := x.GetPrevTableName()
  selectString := x.SelectString(prevTable) // we could pass eui into select for a nicer abstraction
  //fmt.Printf("table.SelectString(): %s\n", selectString)
  results, err := x.db.Query(selectString)
  if err != nil {
    return err, false
  }
  defer results.Close()
  for results.Next() {
    var row PrevRow
    err = results.Scan( &row.Id, &row.Eui, &row.CreatedAt, &row.UpdatedAt, &row.GatewayEui, &row.Tslot,)
    if err != nil {
       return err, false
    }
    x.Id.Load(row.Id)
    x.Eui.Load(row.Eui)
    x.CreatedAt.Load(row.CreatedAt)
    x.UpdatedAt.Load(row.UpdatedAt)
    x.GatewayEui.Load(row.GatewayEui)
    x.Tslot.Load(row.Tslot)
    return nil, true
  }
  return nil, false
}

func (x *Atc_TslotAlloc) CreateTableString() string {
  return "create table " + x.GetCurrentTableName() +
  " ( " + x.Id.String() + " " + x.Id.Type() + " " + x.Id.Modifiers() +
  ", " + x.Eui.String() + " " + x.Eui.Type() + " " + x.Eui.Modifiers() +
  ", " + x.CreatedAt.String() + " " + x.CreatedAt.Type() + " " + x.CreatedAt.Modifiers() +
  ", " + x.UpdatedAt.String() + " " + x.UpdatedAt.Type() + " " + x.UpdatedAt.Modifiers() +
  ", " + x.GatewayEui.String() + " " + x.GatewayEui.Type() + " " + x.GatewayEui.Modifiers() +
  ", " + x.Tslot.String() + " " + x.Tslot.Type() + " " + x.Tslot.Modifiers() +
  ", " + x.constraints + " );"
}

// used internally for creating table name with current or previous version
func tableName(name string, version int) string {
   return name + "_v" + strconv.Itoa(version)
}

func newDeployment(current_version int, previous_version int) bool {
  if current_version == (previous_version + 1) {
    return true;
  }
  return false;
}

func (x *Atc_TslotAlloc) GetCurrentTableName() string {
  return tableName(x.tableName, x.version)
}

func (x *Atc_TslotAlloc) SelectString(tableName string) string {
  if x.txValid {
    return "select * from " + tableName + " where eui = " + x.Eui.QueryValue() + " for update;"
  } else {
    return "select * from " + tableName + " where eui = " + x.Eui.QueryValue() + ";"
  }
}

// insert is only done to the current table
func (x *Atc_TslotAlloc) InsertString() string {
  return "insert into " + x.GetCurrentTableName() +
   " (" + x.Eui.String() + " , " +  x.GatewayEui.String() + " , " +  x.Tslot.String() + ") values" +
   " (?"+ " , ?" + " , ?" + ");"
}

// function returns true if at least one field has been updated
func (x *Atc_TslotAlloc) UpdateString() (string, bool) {
  count := 0
  first := true
  return_str := "update " + x.GetCurrentTableName() + " set "
  if x.Eui.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Eui.String() + " = " + x.Eui.QueryValue()
    first = false
  }
  if x.GatewayEui.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.GatewayEui.String() + " = " + x.GatewayEui.QueryValue()
    first = false
  }
  if x.Tslot.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Tslot.String() + " = " + x.Tslot.QueryValue()
    first = false
  }
  return_str += " where Eui = " + x.Eui.QueryValue() + ";"
  if count > 0 {
    return return_str, true
  } else {
    return "", false
  }
}

func (x *Atc_TslotAlloc) DeleteString(tableName string) string {
  return "delete from " + tableName + " where eui = " + x.Eui.QueryValue() + ";"
}

func (x *Atc_TslotAlloc) DeleteByIdString(tableName string) string {
  return "delete from " + tableName + " where id = " + x.Id.QueryValue() + ";"
}

// function returns true if at least one field has been updated
func (x *Atc_TslotAlloc) UpdateByIdString() (string, bool) {
  count := 0
  first := true
  return_str := "update " + x.GetCurrentTableName() + " set "
  if x.Eui.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Eui.String() + " = " + x.Eui.QueryValue()
    first = false
  }
  if x.GatewayEui.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.GatewayEui.String() + " = " + x.GatewayEui.QueryValue()
    first = false
  }
  if x.Tslot.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Tslot.String() + " = " + x.Tslot.QueryValue()
    first = false
  }
  return_str += " where id = " + x.Id.QueryValue() + ";"
  if count > 0 {
    return return_str, true
  } else {
    return "", false
  }
}

// updated indicates whether any fields have changed and thus an update to the db was attempted 
func (x *Atc_TslotAlloc) UpdateById() (anyFieldModified bool, err error) {
  updateString, anyFieldModified := x.UpdateByIdString()
  if anyFieldModified {
    //fmt.Printf("table.UpdateByIdString(): %s\n", updateString)

    _, err := x.db.Exec(updateString,
    )
    if err != nil {
      return anyFieldModified, err
    }
  } else {
    fmt.Printf("table.UpdateById: No columns modified\n")
  }
  return anyFieldModified, nil
}

func (x *Atc_TslotAlloc) SelectByIdFromCurrent(id int64) (error, bool) {  //d.Id = id
  x.Id.Load(id)
  tableName := x.GetCurrentTableName()
  selectString := x.SelectByIdString(tableName)
  //fmt.Printf("table.SelectByIdString(): %s\n", selectString)
  results, err := x.db.Query(selectString)
  if err != nil {
    return err, false
  }
  defer results.Close()

  for results.Next() {
    var row Row
//err = results.Scan(&row.Id, &row.Eui, &row.SysApi, &row.AppApi)
    err = results.Scan( &row.Id, &row.Eui, &row.CreatedAt, &row.UpdatedAt, &row.GatewayEui, &row.Tslot,)
  if err != nil {
  return err, false
  }
 x.Id.Load(row.Id)
 x.Eui.Load(row.Eui)
 x.CreatedAt.Load(row.CreatedAt)
 x.UpdatedAt.Load(row.UpdatedAt)
 x.GatewayEui.Load(row.GatewayEui)
 x.Tslot.Load(row.Tslot)
    return nil, true
  }
  return nil, false
}

func (x *Atc_TslotAlloc) SelectByIdString(tableName string) string {
  return "select * from " + tableName + " where id = " + x.Id.QueryValue() + ";"
}

