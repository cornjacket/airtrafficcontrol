package tslot

import (
  "strconv"
)

type Tslot struct {
  value int
  updated bool
}

func New() *Tslot {
  return &Tslot{}
}

func (x *Tslot) IsUpdated() bool {
  return x.updated
}

func (x *Tslot) Get() int {
  return x.value
}

func (x *Tslot) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *Tslot) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *Tslot) String() string {
  return "Tslot"
}

func (x *Tslot) Type() string {
  return "int"
}

func (x *Tslot) Modifiers() string {
  return "not null"
}

func (x *Tslot) QueryValue() string {
  return strconv.Itoa(x.value)
}

