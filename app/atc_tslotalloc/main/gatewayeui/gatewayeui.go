package gatewayeui

import (
)

type GatewayEui struct {
  value string
  updated bool
}

func New() *GatewayEui {
  return &GatewayEui{}
}

func (x *GatewayEui) IsUpdated() bool {
  return x.updated
}

func (x *GatewayEui) Get() string {
  return x.value
}

func (x *GatewayEui) Set(value string) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *GatewayEui) Load(value string) {
  x.value = value
  x.updated = false
}

func (x *GatewayEui) String() string {
  return "GatewayEui"
}

func (x *GatewayEui) Type() string {
  return "varchar(22)"
}

func (x *GatewayEui) Modifiers() string {
  return "not null"
}

func (x *GatewayEui) QueryValue() string {
  if x.value == "" {
    return "0"
  }
  return "\"" + x.value + "\""
}

