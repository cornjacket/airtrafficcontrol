package tallocreqproc

import (
	// Standard library packages
	"encoding/json"
	"fmt"
	"net/http"

	// Third party packages
	"bitbucket.org/cornjacket/iot/loriot"
	"github.com/julienschmidt/httprouter"
)

// There is some redundancy here wrt appnum and classId
type TAllocReq struct {
        NodeEui         string
        // DRT - I believe the following info is needed to determine the destination cmd handler to send the response.
        AppNum          int
        AppApi          int
        SysApi          int
        Ts              uint64
        TimeReq         int // number of seconds needed by the Node
        CorrId  	uint64  `json:"corrid"`
        ClassId 	string  `json:"classid"`
        Hops		int     `json:"hops"`
}


func NewTAllocReqHandlerFunc(NWorkers int, fp func(int, TAllocReq) bool) func(http.ResponseWriter, *http.Request, httprouter.Params) {

	// Start the dispatcher.
	fmt.Printf("INFO\tStarting the dispatcher\n")

	StartDispatcher(NWorkers)

	// register data handler
	registerTAllocReqProcHandler(fp) // if i want the function to be attached to the controller instance then i need to attach it to the struct

	return createTAlloc 
}

// CreateData creates a new data resource
// returns the original data so that testing can confirm the data was received - this is not necessary.
func createTAlloc(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	// Stub a data to be populated from the body
	t := TAllocReq{}

	// Populate the data
	json.NewDecoder(r.Body).Decode(&t)

	// Marshal provided interface into JSON strucutre
	tj, _ := json.Marshal(t)

	work := WorkRequest{TAllocReq: t}

	// Push the work onto the queue.
	WorkQueue <- work
	//fmt.Println("Work request queued")
	//fmt.Printf("CREATE\tINFO\tWork Request Queued: %v\n", work)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)
	fmt.Fprintf(w, "%s", tj)
}

func DisplayPassFailMessageWithLatency(message string, err error, wID int, t *TAllocReq, verbose bool) {
        if err != nil {
                fmt.Printf("FAIL, classId: %s, corrId: %d, EUI: %s, TallocReqWorker %d, %s, hops: %d, err: %s\n", t.ClassId, t.CorrId, t.NodeEui, wID, message, t.Hops, err)
        } else if verbose {
                now := loriot.Now()
                latency := now - t.Ts
                fmt.Printf("PASS, classId: %s, corrId: %d, EUI: %s, time: %d, TallocReqWorker %d, tallocReq: %s, hops: %d, ts: %d, ts0: %d, lat: %d\n",
                        t.ClassId, t.CorrId, t.NodeEui, t.TimeReq, wID, message, t.Hops, now, t.Ts, latency)
        }
}

func DisplayInfoMessageWithLatency(message string, wID int, t *TAllocReq) {
        now := loriot.Now()
        latency := now - t.Ts
        fmt.Printf("PASS, classId: %s, corrId: %d, EUI: %s, time: %d, TallocReqWorker %d, tallocReq: %s, hops: %d, ts: %d, ts0: %d, lat: %d\n",
                t.ClassId, t.CorrId, t.NodeEui, t.TimeReq, wID, message, t.Hops, now, t.Ts, latency)
}


