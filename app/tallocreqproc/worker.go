package tallocreqproc

import (
	"fmt"

	//"github.com/cornjacket/iot/message"
)

var tAllocReqProcFunc func(int, TAllocReq) bool
var tAllocReqProcFuncIsSet bool = false

// NewWorker creates, and returns a new Worker object. Its only argument
// is a channel that the worker can add itself to whenever it is done its
// work.
func NewWorker(id int, workerQueue chan chan WorkRequest) Worker {
	// Create, and return the worker.
	worker := Worker{
		ID:          id,
		Work:        make(chan WorkRequest),
		WorkerQueue: workerQueue,
		QuitChan:    make(chan bool)}

	return worker
}

func registerTAllocReqProcHandler(fp func(int, TAllocReq) bool) {
    tAllocReqProcFunc = fp
    tAllocReqProcFuncIsSet = true
    fmt.Printf("RegisterTAllocReqProcHandler invoked\n")
}


type Worker struct {
	ID          int
	Work        chan WorkRequest
	WorkerQueue chan chan WorkRequest
	QuitChan    chan bool
}

// This function "starts" the worker by starting a goroutine, that is
// an infinite "for-select" loop.
func (w *Worker) Start() {
	go func() {
		for {
			// Add ourselves into the worker queue.
			w.WorkerQueue <- w.Work

			select {
			case work := <-w.Work:
				// Receive a work request.
				//fmt.Printf("worker%d: Received TAlloc request %s\n", w.ID, work.TAllocReq)

				if tAllocReqProcFuncIsSet == true {
					//fmt.Printf("worker: pre tAllocReqProcFunc() invoked.\n")
					tAllocReqProcFunc(w.ID, work.TAllocReq)
				} else {
					fmt.Printf("worker%d: ERROR: tAllocReqProcFunc is not set.\n", w.ID)

				}

			case <-w.QuitChan:
				// We have been asked to stop.
				fmt.Printf("worker%d stopping\n", w.ID)
				return
			}
		}
	}()
}

// Stop tells the worker to stop listening for work requests.
//
// Note that the worker will only stop *after* it has finished its work.
func (w *Worker) Stop() {
	go func() {
		w.QuitChan <- true
	}()
}
