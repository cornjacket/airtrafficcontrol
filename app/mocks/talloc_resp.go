package mocks

import (
	"log"
        "bitbucket.org/cornjacket/iot/serverlib/tallocrespproc"
)

type TallocRespServiceMock struct {
}

var AtcLastTallocResp	tallocrespproc.TallocResp	

func (s *TallocRespServiceMock) Open(Hostname, Port, Path string) error {
	log.Println("TallocRespServiceMock.Open() invoked.")
	AtcLastTallocResp = tallocrespproc.TallocResp{} // reset to default value
	return nil
}

func (s *TallocRespServiceMock) SendTallocResp(resp tallocrespproc.TallocResp) (error) {
	log.Println("TallocRespServiceMock.SendTallocResp() invoked.")
	AtcLastTallocResp = resp 
	return nil
}
