package atc_nodes

import (
  // Standard library packets
  "fmt"
  "os"
  "strconv"
  "database/sql"
  _ "github.com/go-sql-driver/mysql"
  "time"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes/main/eui"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes/main/validonboard"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes/main/createdat"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes/main/updatedat"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes/main/validtslot"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes/main/assignedtslot"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes/main/actualtslot"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes/main/primarygateway"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes/main/secondarygateway"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes/main/lastgateway"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes/main/homenetwork"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes/main/subband"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes/main/lastpktrcvd"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes/main/lastpktts"
  "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes/main/tslottype"
)

type Row struct {
  Eui	string	`json:"eui"`
  ValidOnboard	int	`json:"validonboard"`
  CreatedAt	time.Time	`json:"createdat"`
  UpdatedAt	time.Time	`json:"updatedat"`
  ValidTslot	int	`json:"validtslot"`
  AssignedTslot	int	`json:"assignedtslot"`
  ActualTslot	int	`json:"actualtslot"`
  PrimaryGateway	string	`json:"primarygateway"`
  SecondaryGateway	string	`json:"secondarygateway"`
  LastGateway	string	`json:"lastgateway"`
  HomeNetwork	int	`json:"homenetwork"`
  Subband	int	`json:"subband"`
  LastPktRcvd	string	`json:"lastpktrcvd"`
  LastPktTs	int64	`json:"lastpktts"`
  TslotType	int	`json:"tslottype"`
}

type PrevRow struct {
  Eui	string	`json:"eui"`
  ValidOnboard	int	`json:"validonboard"`
  CreatedAt	time.Time	`json:"createdat"`
  UpdatedAt	time.Time	`json:"updatedat"`
  ValidTslot	int	`json:"validtslot"`
  AssignedTslot	int	`json:"assignedtslot"`
  ActualTslot	int	`json:"actualtslot"`
  PrimaryGateway	string	`json:"primarygateway"`
  SecondaryGateway	string	`json:"secondarygateway"`
  LastGateway	string	`json:"lastgateway"`
  HomeNetwork	int	`json:"homenetwork"`
  Subband	int	`json:"subband"`
  LastPktRcvd	string	`json:"lastpktrcvd"`
  LastPktTs	int64	`json:"lastpktts"`
  TslotType	int	`json:"tslottype"`
}

var db_conn *sql.DB

func Init(db *sql.DB) {
 fmt.Printf("Atc_Nodes.Init() invoked\n")
  if db != nil {
    db_conn = db
  } else {
   fmt.Printf("Atc_Nodes.Init() db = nil. Exiting...\n")
    os.Exit(0)
  }
  atc_nodes := New()
  //currentTable := atc_nodes.GetCurrentTableName()
  //fmt.Printf("currentTable: %s, CreateTableName: %s\n", currentTable, atc_nodes.CreateTableString())
  // Create table if it doesn't already exist
  err := atc_nodes.CreateTable()
  if err != nil {
    fmt.Printf("atc_nodes.CreateTable() failed. Assuming already exists.\n")
  } else {
    fmt.Printf("atc_nodes.CreateTable() succeeded!!\n")
  }
}

type Atc_Nodes struct {
  db           *sql.DB
  tx           *sql.Tx // used for the transaction
  txValid      bool // determines whether a transaction is currently in progress
  tableName    string
  prevVersion  int
  version      int
  constraints  string
  Eui	*eui.Eui
  ValidOnboard	*validonboard.ValidOnboard
  CreatedAt	*createdat.CreatedAt
  UpdatedAt	*updatedat.UpdatedAt
  ValidTslot	*validtslot.ValidTslot
  AssignedTslot	*assignedtslot.AssignedTslot
  ActualTslot	*actualtslot.ActualTslot
  PrimaryGateway	*primarygateway.PrimaryGateway
  SecondaryGateway	*secondarygateway.SecondaryGateway
  LastGateway	*lastgateway.LastGateway
  HomeNetwork	*homenetwork.HomeNetwork
  Subband	*subband.Subband
  LastPktRcvd	*lastpktrcvd.LastPktRcvd
  LastPktTs	*lastpktts.LastPktTs
  TslotType	*tslottype.TslotType
}

func New() *Atc_Nodes {
  x := &Atc_Nodes{}
  x.db = db_conn
  x.txValid = false
  x.tableName = "atc_nodes"
  x.prevVersion = 1
  x.version = 2
  x.Eui=	eui.New()
  x.ValidOnboard=	validonboard.New()
  x.CreatedAt=	createdat.New()
  x.UpdatedAt=	updatedat.New()
  x.ValidTslot=	validtslot.New()
  x.AssignedTslot=	assignedtslot.New()
  x.ActualTslot=	actualtslot.New()
  x.PrimaryGateway=	primarygateway.New()
  x.SecondaryGateway=	secondarygateway.New()
  x.LastGateway=	lastgateway.New()
  x.HomeNetwork=	homenetwork.New()
  x.Subband=	subband.New()
  x.LastPktRcvd=	lastpktrcvd.New()
  x.LastPktTs=	lastpktts.New()
  x.TslotType=	tslottype.New()
  x.constraints = "constraint pk_example primary key (eui)"
  return x
}

var prevTableExists bool = false // TODO: Create Mechanism to auto detect if prevTable exists

func PrevTableExists() bool {
  return prevTableExists;
}

// Close() should be called after all DB operations are done. If a transaction is currently in progress, then Close will Rollback or Commit based on the DB error
// sent to Close(). If Close or Rollback has already been called by the user, then calling Close() should have no effect.
func (x *Atc_Nodes) Close (err error) (error, bool) {
  if x.txValid {
    if err != nil {
      return x.tx.Rollback(), true
    } else {
      return x.tx.Commit(), true
    }
  }
  return nil, false
}

// If transaction is in progress, then turn off the transaction state and call rollback, releasing the lock
func (x *Atc_Nodes) Rollback () (error, bool) {
  if x.txValid {
    x.txValid = false
    return x.tx.Rollback(), true
  }
  return nil, false
}

// If transaction is in progress, then turn off the transaction state and call commit, releasing the lock
func (x *Atc_Nodes) Commit() (error, bool) {
  if x.txValid {
    x.txValid = false
    return x.tx.Commit(), true
  }
  return nil, false
}

func (x *Atc_Nodes) GetByEui (eui string, createIfNotFound bool, lockRow bool) (error, bool) {
  //fmt.Printf("table.GetByEui(): Entrance.\n")
  x.Eui.Load(eui)  // required: all rows must have a valid_onboard field that defaults to false, used to indicate if a field was created properly.
  // getByEui should select/search for entry in current version table
  err, rcvd := x.SelectFromCurrent(eui, lockRow)
  // if entryfound then process_normally
  if err == nil && rcvd == true {
    //fmt.Printf("table.GetByEui(): Returning nil, no error, rcvd one entry..\n")
    return nil, true
  }
  // else // no entry found
  //fmt.Printf("table.GetByEui(): row DNE in current table.\n")
  if createIfNotFound == true {
    if PrevTableExists() {
      //fmt.Printf("table.GetByEui(): checking prev table\n")
      err, rcvd = x.SelectFromPrev(eui)
      entryFound := (err == nil && rcvd == true)
      if entryFound {
//       copy over parameters from old row to new row, along with new row's new default fields, with valid_onboard set to true
        x.HandleMigrationOrRollback()
        err = x.insert()
        if err != nil {
          //fmt.Printf("table.GetByEui(): x.insert failed.\n")
          return err, rcvd
        }
        //fmt.Printf("table.GetByEui(): x.insert succeeded.\n")
        err = x.DeleteByEui(x.GetPrevTableName())
        if err != nil {
          //fmt.Printf("table.GetByEui(): x.delete %s failed from PrevTable.\n")
          return err, rcvd
        }
      } else {
//       create default row with valid_onboard = false -- How to do this?
        err = x.insert() // later we can just return this i think
        if err != nil {
          //fmt.Printf("table.GetByEui(): x.insert failed.\n")
          return err, rcvd
        }
//       create default row with valid_onboard = false
      }
    } else { // later we can just return this i think
      err = x.insert()
      if err != nil {
        //fmt.Printf("table.GetByEui(): x.insert failed.\n")
        return err, rcvd
      }
    }
    // TODO: If lockRow, then SelectFromCurrent (with lock on) should be called again since now the row has been created... otherwise there is a potential race condition
  }
  return err, rcvd
}

func (x *Atc_Nodes) HandleMigrationOrRollback() {
// currently doing nothing but this is where the migration or rollback logic should happen
// this should be generated code based on whether migration or rollback
fmt.Printf("table.HandleColumnsForMigrationOrRollback(): \n")
}

func (x *Atc_Nodes) CreateTable() error {
  _, err := x.db.Exec(x.CreateTableString())
  return err
}

func (x *Atc_Nodes) DeleteByEui(tableName string) error {
  _, err := x.db.Exec(x.DeleteString(tableName))
  return err
}

func (x *Atc_Nodes) Insert(row *Row) error {
  x.Eui.Load(row.Eui)
  x.ValidOnboard.Load(row.ValidOnboard)
  x.ValidTslot.Load(row.ValidTslot)
  x.AssignedTslot.Load(row.AssignedTslot)
  x.ActualTslot.Load(row.ActualTslot)
  x.PrimaryGateway.Load(row.PrimaryGateway)
  x.SecondaryGateway.Load(row.SecondaryGateway)
  x.LastGateway.Load(row.LastGateway)
  x.HomeNetwork.Load(row.HomeNetwork)
  x.Subband.Load(row.Subband)
  x.LastPktRcvd.Load(row.LastPktRcvd)
  x.LastPktTs.Load(row.LastPktTs)
  x.TslotType.Load(row.TslotType)
  return x.insert()
}

func (x *Atc_Nodes) insert() error {
  var err error
  insertString := x.InsertString()
  //fmt.Printf("table.InsertString(): %s\n", insertString)
  if x.txValid {
  	_, err = x.tx.Exec(insertString,
   	x.Eui.Get(),
   	x.ValidOnboard.Get(),
   	x.ValidTslot.Get(),
   	x.AssignedTslot.Get(),
   	x.ActualTslot.Get(),
   	x.PrimaryGateway.Get(),
   	x.SecondaryGateway.Get(),
   	x.LastGateway.Get(),
   	x.HomeNetwork.Get(),
   	x.Subband.Get(),
   	x.LastPktRcvd.Get(),
   	x.LastPktTs.Get(),
   	x.TslotType.Get(),
  	)
  } else {
  	_, err = x.db.Exec(insertString,
   	x.Eui.Get(),
   	x.ValidOnboard.Get(),
   	x.ValidTslot.Get(),
   	x.AssignedTslot.Get(),
   	x.ActualTslot.Get(),
   	x.PrimaryGateway.Get(),
   	x.SecondaryGateway.Get(),
   	x.LastGateway.Get(),
   	x.HomeNetwork.Get(),
   	x.Subband.Get(),
   	x.LastPktRcvd.Get(),
   	x.LastPktTs.Get(),
   	x.TslotType.Get(),
  	)
  }
  if err != nil {
    return err
  }
  return nil
}

// updated indicates whether any fields have changed and thus an update to the db was attempted 
func (x *Atc_Nodes) Update() (anyFieldModified bool, err error) {
  updateString, anyFieldModified := x.UpdateString()
  //fmt.Printf("table.UpdateString(): %s\n", updateString)

  if anyFieldModified {
    if x.txValid {
      _, err = x.tx.Exec(updateString)
    } else {
      _, err = x.db.Exec(updateString)
    }
  }
  if err != nil {
    return anyFieldModified, err
  }
  return anyFieldModified, nil
}

func (x *Atc_Nodes) SelectFromCurrent(eui string, lockRow bool) (error, bool) { // later this will be SelectByEui(eui string)
  //d.Id = id
  x.Eui.Load(eui)
  tableName := x.GetCurrentTableName()
  var err error
  var tx *sql.Tx
  x.txValid = lockRow
  selectString := x.SelectString(tableName) // we could pass eui into select for a nicer abstraction
  //fmt.Printf("table.SelectString(): %s\n", selectString)

  if x.txValid {
    tx, err = x.db.Begin()
    x.tx = tx
    if err != nil {
      //fmt.Printf("selectForCurrent x.db.Begin() failed\n")
      return err, false
    }
  }

  var results *sql.Rows
  if x.txValid {
    results, err = tx.Query(selectString)
  } else {
    results, err = x.db.Query(selectString)
  }
  if err != nil {
    //fmt.Printf("tx.Query 1 fail\n")
    return err, false
  } else {
    defer results.Close()

    //fmt.Printf("tx.Query 1 pass\n")
  }


  for results.Next() {
    var row Row
//err = results.Scan(&row.Id, &row.Eui, &row.SysApi, &row.AppApi)
    err = results.Scan( &row.Eui, &row.ValidOnboard, &row.CreatedAt, &row.UpdatedAt, &row.ValidTslot, &row.AssignedTslot, &row.ActualTslot, &row.PrimaryGateway, &row.SecondaryGateway, &row.LastGateway, &row.HomeNetwork, &row.Subband, &row.LastPktRcvd, &row.LastPktTs, &row.TslotType,)
  if err != nil {
  return err, false
  }
 x.Eui.Load(row.Eui)
 x.ValidOnboard.Load(row.ValidOnboard)
 x.CreatedAt.Load(row.CreatedAt)
 x.UpdatedAt.Load(row.UpdatedAt)
 x.ValidTslot.Load(row.ValidTslot)
 x.AssignedTslot.Load(row.AssignedTslot)
 x.ActualTslot.Load(row.ActualTslot)
 x.PrimaryGateway.Load(row.PrimaryGateway)
 x.SecondaryGateway.Load(row.SecondaryGateway)
 x.LastGateway.Load(row.LastGateway)
 x.HomeNetwork.Load(row.HomeNetwork)
 x.Subband.Load(row.Subband)
 x.LastPktRcvd.Load(row.LastPktRcvd)
 x.LastPktTs.Load(row.LastPktTs)
 x.TslotType.Load(row.TslotType)
    return nil, true
  }
  return nil, false
}

func (x *Atc_Nodes) GetPrevTableName() string {
  return tableName(x.tableName, x.prevVersion)
}

//func (x *Atc_Nodes) Select(id int64) error { // later this will be SelectByEui(eui string)
func (x *Atc_Nodes) SelectFromPrev(eui string) (error, bool)  { // later this will be SelectByEui(eui string)
  //x.Id = id
  x.Eui.Load(eui)
  prevTable := x.GetPrevTableName()
  selectString := x.SelectString(prevTable) // we could pass eui into select for a nicer abstraction
  //fmt.Printf("table.SelectString(): %s\n", selectString)
  results, err := x.db.Query(selectString)
  if err != nil {
    return err, false
  }
  defer results.Close()
  for results.Next() {
    var row PrevRow
    err = results.Scan( &row.Eui, &row.ValidOnboard, &row.CreatedAt, &row.UpdatedAt, &row.ValidTslot, &row.AssignedTslot, &row.ActualTslot, &row.PrimaryGateway, &row.SecondaryGateway, &row.LastGateway, &row.HomeNetwork, &row.Subband, &row.LastPktRcvd, &row.LastPktTs, &row.TslotType,)
    if err != nil {
       return err, false
    }
    x.Eui.Load(row.Eui)
    x.ValidOnboard.Load(row.ValidOnboard)
    x.CreatedAt.Load(row.CreatedAt)
    x.UpdatedAt.Load(row.UpdatedAt)
    x.ValidTslot.Load(row.ValidTslot)
    x.AssignedTslot.Load(row.AssignedTslot)
    x.ActualTslot.Load(row.ActualTslot)
    x.PrimaryGateway.Load(row.PrimaryGateway)
    x.SecondaryGateway.Load(row.SecondaryGateway)
    x.LastGateway.Load(row.LastGateway)
    x.HomeNetwork.Load(row.HomeNetwork)
    x.Subband.Load(row.Subband)
    x.LastPktRcvd.Load(row.LastPktRcvd)
    x.LastPktTs.Load(row.LastPktTs)
    x.TslotType.Load(row.TslotType)
    return nil, true
  }
  return nil, false
}

func (x *Atc_Nodes) CreateTableString() string {
  return "create table " + x.GetCurrentTableName() +
  " ( " + x.Eui.String() + " " + x.Eui.Type() + " " + x.Eui.Modifiers() +
  ", " + x.ValidOnboard.String() + " " + x.ValidOnboard.Type() + " " + x.ValidOnboard.Modifiers() +
  ", " + x.CreatedAt.String() + " " + x.CreatedAt.Type() + " " + x.CreatedAt.Modifiers() +
  ", " + x.UpdatedAt.String() + " " + x.UpdatedAt.Type() + " " + x.UpdatedAt.Modifiers() +
  ", " + x.ValidTslot.String() + " " + x.ValidTslot.Type() + " " + x.ValidTslot.Modifiers() +
  ", " + x.AssignedTslot.String() + " " + x.AssignedTslot.Type() + " " + x.AssignedTslot.Modifiers() +
  ", " + x.ActualTslot.String() + " " + x.ActualTslot.Type() + " " + x.ActualTslot.Modifiers() +
  ", " + x.PrimaryGateway.String() + " " + x.PrimaryGateway.Type() + " " + x.PrimaryGateway.Modifiers() +
  ", " + x.SecondaryGateway.String() + " " + x.SecondaryGateway.Type() + " " + x.SecondaryGateway.Modifiers() +
  ", " + x.LastGateway.String() + " " + x.LastGateway.Type() + " " + x.LastGateway.Modifiers() +
  ", " + x.HomeNetwork.String() + " " + x.HomeNetwork.Type() + " " + x.HomeNetwork.Modifiers() +
  ", " + x.Subband.String() + " " + x.Subband.Type() + " " + x.Subband.Modifiers() +
  ", " + x.LastPktRcvd.String() + " " + x.LastPktRcvd.Type() + " " + x.LastPktRcvd.Modifiers() +
  ", " + x.LastPktTs.String() + " " + x.LastPktTs.Type() + " " + x.LastPktTs.Modifiers() +
  ", " + x.TslotType.String() + " " + x.TslotType.Type() + " " + x.TslotType.Modifiers() +
  ", " + x.constraints + " );"
}

// used internally for creating table name with current or previous version
func tableName(name string, version int) string {
   return name + "_v" + strconv.Itoa(version)
}

func newDeployment(current_version int, previous_version int) bool {
  if current_version == (previous_version + 1) {
    return true;
  }
  return false;
}

func (x *Atc_Nodes) GetCurrentTableName() string {
  return tableName(x.tableName, x.version)
}

func (x *Atc_Nodes) SelectString(tableName string) string {
  if x.txValid {
    return "select * from " + tableName + " where eui = " + x.Eui.QueryValue() + " for update;"
  } else {
    return "select * from " + tableName + " where eui = " + x.Eui.QueryValue() + ";"
  }
}

// insert is only done to the current table
func (x *Atc_Nodes) InsertString() string {
  return "insert into " + x.GetCurrentTableName() +
   " (" + x.Eui.String() + " , " +  x.ValidOnboard.String() + " , " +  x.ValidTslot.String() + " , " +  x.AssignedTslot.String() + " , " +  x.ActualTslot.String() + " , " +  x.PrimaryGateway.String() + " , " +  x.SecondaryGateway.String() + " , " +  x.LastGateway.String() + " , " +  x.HomeNetwork.String() + " , " +  x.Subband.String() + " , " +  x.LastPktRcvd.String() + " , " +  x.LastPktTs.String() + " , " +  x.TslotType.String() + ") values" +
   " (?"+ " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + " , ?" + ");"
}

// function returns true if at least one field has been updated
func (x *Atc_Nodes) UpdateString() (string, bool) {
  count := 0
  first := true
  return_str := "update " + x.GetCurrentTableName() + " set "
  if x.ValidOnboard.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ValidOnboard.String() + " = " + x.ValidOnboard.QueryValue()
    first = false
  }
  if x.ValidTslot.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ValidTslot.String() + " = " + x.ValidTslot.QueryValue()
    first = false
  }
  if x.AssignedTslot.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.AssignedTslot.String() + " = " + x.AssignedTslot.QueryValue()
    first = false
  }
  if x.ActualTslot.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ActualTslot.String() + " = " + x.ActualTslot.QueryValue()
    first = false
  }
  if x.PrimaryGateway.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.PrimaryGateway.String() + " = " + x.PrimaryGateway.QueryValue()
    first = false
  }
  if x.SecondaryGateway.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.SecondaryGateway.String() + " = " + x.SecondaryGateway.QueryValue()
    first = false
  }
  if x.LastGateway.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastGateway.String() + " = " + x.LastGateway.QueryValue()
    first = false
  }
  if x.HomeNetwork.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.HomeNetwork.String() + " = " + x.HomeNetwork.QueryValue()
    first = false
  }
  if x.Subband.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Subband.String() + " = " + x.Subband.QueryValue()
    first = false
  }
  if x.LastPktRcvd.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastPktRcvd.String() + " = " + x.LastPktRcvd.QueryValue()
    first = false
  }
  if x.LastPktTs.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.LastPktTs.String() + " = " + x.LastPktTs.QueryValue()
    first = false
  }
  if x.TslotType.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.TslotType.String() + " = " + x.TslotType.QueryValue()
    first = false
  }
  return_str += " where Eui = " + x.Eui.QueryValue() + ";"
  if count > 0 {
    return return_str, true
  } else {
    return "", false
  }
}

func (x *Atc_Nodes) DeleteString(tableName string) string {
  return "delete from " + tableName + " where eui = " + x.Eui.QueryValue() + ";"
}

