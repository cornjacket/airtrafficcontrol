package subband

import (
  "strconv"
)

type Subband struct {
  value int
  updated bool
}

func New() *Subband {
  return &Subband{}
}

func (x *Subband) IsUpdated() bool {
  return x.updated
}

func (x *Subband) Get() int {
  return x.value
}

func (x *Subband) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *Subband) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *Subband) String() string {
  return "Subband"
}

func (x *Subband) Type() string {
  return "int"
}

func (x *Subband) Modifiers() string {
  return "not null"
}

func (x *Subband) QueryValue() string {
  return strconv.Itoa(x.value)
}

