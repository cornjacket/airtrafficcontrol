package secondarygateway

import (
)

type SecondaryGateway struct {
  value string
  updated bool
}

func New() *SecondaryGateway {
  return &SecondaryGateway{}
}

func (x *SecondaryGateway) IsUpdated() bool {
  return x.updated
}

func (x *SecondaryGateway) Get() string {
  return x.value
}

func (x *SecondaryGateway) Set(value string) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *SecondaryGateway) Load(value string) {
  x.value = value
  x.updated = false
}

func (x *SecondaryGateway) String() string {
  return "SecondaryGateway"
}

func (x *SecondaryGateway) Type() string {
  return "varchar(22)"
}

func (x *SecondaryGateway) Modifiers() string {
  return "not null"
}

func (x *SecondaryGateway) QueryValue() string {
  if x.value == "" {
    return "0"
  }
  return "\"" + x.value + "\""
}

