package lastpktts

import (
  "strconv"
)

type LastPktTs struct {
  value int64
  updated bool
}

func New() *LastPktTs {
  return &LastPktTs{}
}

func (x *LastPktTs) IsUpdated() bool {
  return x.updated
}

func (x *LastPktTs) Get() int64 {
  return x.value
}

func (x *LastPktTs) Set(value int64) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *LastPktTs) Load(value int64) {
  x.value = value
  x.updated = false
}

func (x *LastPktTs) String() string {
  return "LastPktTs"
}

func (x *LastPktTs) Type() string {
  return "bigint"
}

func (x *LastPktTs) Modifiers() string {
  return "not null"
}

func (x *LastPktTs) QueryValue() string {
  return strconv.FormatInt(x.value, 10)
}

