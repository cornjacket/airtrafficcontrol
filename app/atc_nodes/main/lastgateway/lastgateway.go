package lastgateway

import (
)

type LastGateway struct {
  value string
  updated bool
}

func New() *LastGateway {
  return &LastGateway{}
}

func (x *LastGateway) IsUpdated() bool {
  return x.updated
}

func (x *LastGateway) Get() string {
  return x.value
}

func (x *LastGateway) Set(value string) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *LastGateway) Load(value string) {
  x.value = value
  x.updated = false
}

func (x *LastGateway) String() string {
  return "LastGateway"
}

func (x *LastGateway) Type() string {
  return "varchar(22)"
}

func (x *LastGateway) Modifiers() string {
  return "not null"
}

func (x *LastGateway) QueryValue() string {
  if x.value == "" {
    return "0"
  }
  return "\"" + x.value + "\""
}

