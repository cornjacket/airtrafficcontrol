package validtslot

import (
  "strconv"
)

type ValidTslot struct {
  value int
  updated bool
}

func New() *ValidTslot {
  return &ValidTslot{}
}

func (x *ValidTslot) IsUpdated() bool {
  return x.updated
}

func (x *ValidTslot) Get() int {
  return x.value
}

func (x *ValidTslot) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *ValidTslot) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *ValidTslot) String() string {
  return "ValidTslot"
}

func (x *ValidTslot) Type() string {
  return "int"
}

func (x *ValidTslot) Modifiers() string {
  return "not null"
}

func (x *ValidTslot) QueryValue() string {
  return strconv.Itoa(x.value)
}

