package homenetwork

import (
  "strconv"
)

type HomeNetwork struct {
  value int
  updated bool
}

func New() *HomeNetwork {
  return &HomeNetwork{}
}

func (x *HomeNetwork) IsUpdated() bool {
  return x.updated
}

func (x *HomeNetwork) Get() int {
  return x.value
}

func (x *HomeNetwork) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *HomeNetwork) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *HomeNetwork) String() string {
  return "HomeNetwork"
}

func (x *HomeNetwork) Type() string {
  return "int"
}

func (x *HomeNetwork) Modifiers() string {
  return "not null"
}

func (x *HomeNetwork) QueryValue() string {
  return strconv.Itoa(x.value)
}

