package assignedtslot

import (
  "strconv"
)

type AssignedTslot struct {
  value int
  updated bool
}

func New() *AssignedTslot {
  return &AssignedTslot{}
}

func (x *AssignedTslot) IsUpdated() bool {
  return x.updated
}

func (x *AssignedTslot) Get() int {
  return x.value
}

func (x *AssignedTslot) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *AssignedTslot) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *AssignedTslot) String() string {
  return "AssignedTslot"
}

func (x *AssignedTslot) Type() string {
  return "int"
}

func (x *AssignedTslot) Modifiers() string {
  return "not null"
}

func (x *AssignedTslot) QueryValue() string {
  return strconv.Itoa(x.value)
}

