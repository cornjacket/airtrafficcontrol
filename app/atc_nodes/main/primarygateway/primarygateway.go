package primarygateway

import (
)

type PrimaryGateway struct {
  value string
  updated bool
}

func New() *PrimaryGateway {
  return &PrimaryGateway{}
}

func (x *PrimaryGateway) IsUpdated() bool {
  return x.updated
}

func (x *PrimaryGateway) Get() string {
  return x.value
}

func (x *PrimaryGateway) Set(value string) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *PrimaryGateway) Load(value string) {
  x.value = value
  x.updated = false
}

func (x *PrimaryGateway) String() string {
  return "PrimaryGateway"
}

func (x *PrimaryGateway) Type() string {
  return "varchar(22)"
}

func (x *PrimaryGateway) Modifiers() string {
  return "not null"
}

func (x *PrimaryGateway) QueryValue() string {
  if x.value == "" {
    return "0"
  }
  return "\"" + x.value + "\""
}

