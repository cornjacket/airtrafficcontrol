package actualtslot

import (
  "strconv"
)

type ActualTslot struct {
  value int
  updated bool
}

func New() *ActualTslot {
  return &ActualTslot{}
}

func (x *ActualTslot) IsUpdated() bool {
  return x.updated
}

func (x *ActualTslot) Get() int {
  return x.value
}

func (x *ActualTslot) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *ActualTslot) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *ActualTslot) String() string {
  return "ActualTslot"
}

func (x *ActualTslot) Type() string {
  return "int"
}

func (x *ActualTslot) Modifiers() string {
  return "not null"
}

func (x *ActualTslot) QueryValue() string {
  return strconv.Itoa(x.value)
}

