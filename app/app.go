package app

import (
        "fmt"
        "log"
        "os"
        "database/sql"
        _ "github.com/go-sql-driver/mysql"
        atc_gateways "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_gateways"
        atc_nodes "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_nodes"
        atc_tslotalloc "bitbucket.org/cornjacket/airtrafficcontrol/app/atc_tslotalloc"
        "bitbucket.org/cornjacket/airtrafficcontrol/app/controllers"
	"bitbucket.org/cornjacket/airtrafficcontrol/app/utils/env"
	"bitbucket.org/cornjacket/airtrafficcontrol/app/utils/database"
)


type AppService struct {
        Env             AppEnv
        Context         *controllers.AppContext
        Db              *sql.DB // this is also maintained inside the package libs. need to use for closing the db when testing.
}

type AppEnv struct {
        DbPassword      string
        DbHostname      string
        DbUsername      string
        DbPort          string
        DbName          string
        PortNum         string
}

func NewAppService() AppService {
        a := AppService{}
        a.Context = controllers.NewAppContext()
        return a
}

func (a *AppService) InitEnv() {

        a.Env.DbPassword = env.GetVar("DB_PASSWORD", "", true)
        a.Env.DbHostname = env.GetVar("DB_HOSTNAME", "localhost", false)
        a.Env.DbUsername = env.GetVar("DB_USERNAME", "root", false)
        a.Env.DbPort = env.GetVar("DB_PORT", "3306", false)
        a.Env.DbName = env.GetVar("DB_NAME", "test", false)
        a.Env.PortNum = env.GetVar("ATC_PORT_NUM", "8083", false)

}


func(a *AppService) SetupExternalServices() {

	tallocRespHost := env.GetVar("TALLOC_RESP_HOSTNAME", "localhost", false)
	tallocRespPort := env.GetVar("TALLOC_RESP_PORT", "8080", false)
	tallocRespPath := env.GetVar("TALLOC_RESP_PATH", "/tallocResp.4.3.6", false)

        // not sure if I am goin to use discovery with the CQRS pattern...
        //discoveryHostname := env.GetVar("DISCOVERY_HOSTNAME", "", false)
        //discoveryPort := env.GetVar("DISCOVERY_PORT", "8001", false)

        if a.Context == nil {
                log.Fatal("AppService.Context == nil. Quitting\n")
        }

	// This is a contrived implementation of the TallocRespService, which only has support for one client that must be known at compile time.
        var err error
        if err = a.Context.TallocRespService.Open(tallocRespHost, tallocRespPort, tallocRespPath); err != nil {
                log.Fatal("TallocRespService.Open() error: ", err)
        }

}

func(a *AppService) Init() {
        a.InitEnv()
        a.DropTables()
        a.SetupDatabase()
        a.SetupExternalServices()
}

func (a *AppService) Run() {

        checkIfProjectNeedsBuild() // will this work anymore now that build file will be in subfolder?
        if a.Context == nil {
                log.Fatal("AppService.Context == nil. Quitting\n")
        }
        a.Context.Run(a.Env.PortNum)

}

func (a *AppService) DropTables() {

        fmt.Printf("-------------------------- Tables dropped invoked.\n")
        tableName := atc_nodes.New().GetCurrentTableName()
        err := database.DropTableIfExists(a.Env.DbName, a.Env.DbUsername, a.Env.DbPassword, a.Env.DbHostname, a.Env.DbPort, tableName)
        if err != nil {
                fmt.Printf("init\tdropTable() failed: %s\n", tableName)
                panic(err.Error())
        }
        tableName = atc_gateways.New().GetCurrentTableName()
        err = database.DropTableIfExists(a.Env.DbName, a.Env.DbUsername, a.Env.DbPassword, a.Env.DbHostname, a.Env.DbPort, tableName)
        if err != nil {
                fmt.Printf("init\tdropTable() failed: %s\n", tableName)
                panic(err.Error())
        }
        tableName = atc_tslotalloc.New().GetCurrentTableName()
        err = database.DropTableIfExists(a.Env.DbName, a.Env.DbUsername, a.Env.DbPassword, a.Env.DbHostname, a.Env.DbPort, tableName)
        if err != nil {
                fmt.Printf("init\tdropTable() failed: %s\n", tableName)
                panic(err.Error())
        }
}

func (a *AppService) SetupDatabase() {

        err := database.CreateDatabaseIfNotExists(a.Env.DbName, a.Env.DbUsername, a.Env.DbPassword, a.Env.DbHostname, a.Env.DbPort)
        if err != nil {
                fmt.Printf("init\tcreateDatabase() failed: %s\n", a.Env.DbName)
                panic(err.Error())
        }

        dbConnString := a.Env.DbUsername + ":XXXX"+"@tcp("+a.Env.DbHostname+":"+a.Env.DbPort+")/"+a.Env.DbName+"?parseTime=true"
        //fmt.Printf("init\tdebug: %s\n", dbConnString)
        dbConnString = a.Env.DbUsername + ":"+a.Env.DbPassword+"@tcp("+a.Env.DbHostname+":"+a.Env.DbPort+")/"+a.Env.DbName+"?parseTime=true"

        a.Db, err = sql.Open("mysql", dbConnString)
        if err != nil {
                panic(err.Error())
        }
        a.CreateTables() // TODO(drt) What about err return value?
}

// TEST function - side effect of Init functions is to create the table if it doesn't already exist. This should be used for component testing
// TODO(drt) - these init functions need to return an error that can be checked.
// Init will also seet the internal package level db_conn variable that each database package uses..
func (a *AppService) CreateTables() {
        fmt.Printf("-------------------------- Tables created invoked.\n")
        atc_gateways.Init(a.Db)
        atc_nodes.Init(a.Db)
        atc_tslotalloc.Init(a.Db)
}

func checkIfProjectNeedsBuild() {
	if _, err := os.Stat("./build.json"); err == nil {
		fmt.Printf("Project config has changed but build was not invoked. Run \"guild Build\"\n")
		os.Exit(1)
	}
}
