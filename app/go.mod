module bitbucket.org/cornjacket/airtrafficcontrol/app

go 1.13

require (
	bitbucket.org/cornjacket/airtrafficcontrol/client v0.1.3
	bitbucket.org/cornjacket/event_hndlr/app v0.1.0
	bitbucket.org/cornjacket/iot v0.1.6
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/context v1.1.1
	github.com/julienschmidt/httprouter v1.3.0
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f
)
