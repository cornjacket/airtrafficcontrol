module bitbucket.org/cornjacket/airtrafficcontrol

go 1.13

require (
	bitbucket.org/cornjacket/airtrafficcontrol/app v0.1.2
	bitbucket.org/cornjacket/iot v0.1.6
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/context v1.1.1
	github.com/julienschmidt/httprouter v1.3.0
)
