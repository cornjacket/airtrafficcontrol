package client 

import (

	"fmt"
	"encoding/json"
	"bytes"
	"bitbucket.org/cornjacket/iot/message"

)

type AirTrafficControlService struct {
	Hostname  	string
	Port		string
	PacketPath	string
	TallocReqPath	string
	Enabled		bool
}

// TODO: Add error return value
// TODO: Validate Hostname and Port within reason
func (s *AirTrafficControlService) Open(Hostname, Port, PacketPath, TallocReqPath string) error {
	s.Hostname = Hostname
	s.Port = Port
	s.PacketPath = PacketPath
	s.TallocReqPath = TallocReqPath
	s.Enabled = true
	return s.Ping()	
}

func (s *AirTrafficControlService) URL(path string) string {
	if s.Enabled {
		return "http://" + s.Hostname + ":" + s.Port + path 
	}
	return ""
}

// TODO: This needs to be done...
// Used to determine if the dependent service is currently up.
// Ping should make multiple attempts to see if the external service is up before giving up and returning an error.
func (s *AirTrafficControlService) Ping() (error) {
	// check if enabled
	// if so, then make a GET call to /ping of the URL()
	// if there is an error, then repeatedly call for X times with linear backoff until success
/* Keep an add to services...
        // ping loop
        var pingErr error
        for i := 0; i < 15; i++ {
                fmt.Printf("Pinging %s\n", DbHost)
                pingErr = server.DB.DB().Ping()
                if pingErr != nil {
                        fmt.Println(pingErr)
                } else {
                        fmt.Println("Ping replied.")
                        break;
                }
                time.Sleep(time.Duration(3) * time.Second)
        }
        if pingErr != nil {
                log.Fatal("This is the error:", err)
        }
*/
	return nil
}

func (s *AirTrafficControlService) SendPacket(packet message.UpPacket) (error) {

	var err error
	if s.Enabled {
		//fmt.Printf("AirTrafficControlService: Sending packet to %s\n", s.URL(s.PacketPath))
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(packet)
		err = Post(s.URL(s.PacketPath), b)
	} else {
		fmt.Printf("AirTrafficControl is disabled. Dropping packet.\n")
	}
	return err 

}

// TODO: Should this structure should be moved out of here. It is being replicated. Should this be in a separate package from both
//       the client and app folders?
// This code is copied from airtrafficcontrol allowing the code to compile without depending on having airtrafficcontrol repo checked out.
type TAllocReq struct {
        NodeEui         string
        // DRT - I believe the following info is needed to determine the destination cmd handler to send the response.
        AppNum          int
        AppApi          int
        SysApi          int
        Ts    		uint64
        TimeReq         int // number of seconds needed by the Node
        CorrId  uint64  `json:"corrid"`
        ClassId string  `json:"classid"`
        Hops	int	`json:"hops"`
}

//func (s *AirTrafficControlService)  RequestTslot(p *message.UpPacket) (error) {
func (s *AirTrafficControlService)  RequestTslot(tallocreq *TAllocReq) (error) {

	// todo(DRT): Should this function check for appapi and sysapi?
	//tallocreq := TAllocReq{NodeEui: p.Eui, AppNum: 4, AppApi: 3, SysApi: 6, TimeReq: 15, CorrId: p.CorrId, ClassId: p.ClassId, Hops: p.Hops, Ts: p.Ts}
	var err error
	if s.Enabled {
		fmt.Printf("AirTrafficControlService: Sending tallocreq to %s\n", s.URL(s.TallocReqPath))
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(tallocreq)
		err = Post(s.URL(s.TallocReqPath), b)
	} else {
		fmt.Printf("AirTrafficControl is disabled. Dropping tallocreq.\n")
	}
	return err 

}
