package main

import (
	"bitbucket.org/cornjacket/airtrafficcontrol/app"
)

func main() {

        atcApp := app.NewAppService()
        atcApp.Init()
        atcApp.Run()
}
