#!/bin/bash

# this should originate from the cmd_handler but for testing we will spoof it here

curl -XPOST -H 'Content-Type: application/json' -d '{"dr": "SF10 BW125 4/5", "ts": 1529525468008, "EUI": "000000000000123b", "ack": false, "cmd": "gw", "data": "0c03000228ffff23ea58b9", "fcnt": 1115, "freq": 90230000, "port": 1, "gws": [ { "rssi": -130, "snr": 1.2, "ts": 43424140, "gweui": "1122334455", "lat": 47.2846, "lon": 8.565 } ] }' http://localhost:8083/packet 
